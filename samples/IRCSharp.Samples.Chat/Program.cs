﻿using IRCSharp;
using IRCSharp.Messaging;

const string channel = "#12312";
var irc = IrcClient.New("TestIRC_Client", "irc.rizon.net").Build();

irc.MessageReceived += (_, args) => Console.WriteLine(args.Message);

await irc.Connect();
await irc.Send(new IrcCommand.JOIN(new[] { channel }));

while (true)
{
	string? input = Console.ReadLine();
	if (string.IsNullOrEmpty(input)) continue;

	if (input == "/quit")
	{
		await irc.Close();
		break;
	}

	await irc.Send(new IrcCommand.PRIVMSG(channel, input));
}