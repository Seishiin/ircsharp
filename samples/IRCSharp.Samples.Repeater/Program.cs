﻿using IRCSharp;
using IRCSharp.EventArgs;
using IRCSharp.Messaging;

const string nick = "TestIRC_Client";
const string channel = "#12312";
var irc = IrcClient.New(nick, "irc.rizon.net").Build();

irc.MessageReceived += RepeatMessage;

await irc.Connect();
await irc.Send(new IrcCommand.JOIN(new[] { channel }));

while (true)
{
	string? input = Console.ReadLine();
	if (input == "/quit")
	{
		await irc.Close();
		break;
	}
}

async void RepeatMessage(object? sender, MessageReceivedEventArgs args)
{
	if (args.Message.Command is IrcCommand.PRIVMSG { target: nick } cmd)
	{
		string target = args.Message.ResponseTarget() ?? nick;
		await irc.Send(new IrcCommand.PRIVMSG(target, cmd.text));
	}
}