﻿// ReSharper disable InconsistentNaming

using IRCSharp;
using IRCSharp.Messaging;

var irc = IrcClient.New("TestIRC_Client", "irc.rizon.net")
                   .RegisterCommand<SAJOIN>()
                   .Build();

// SAJOIN [(nick)] (channels)
public record SAJOIN(IEnumerable<string> channels, string? nick = null) : IrcCommand, IIrcCommand
{
	public static string Identifier() => "SAJOIN";
	public override string ToCommandString() => Stringify<SAJOIN>(nick, channels.Merge(','));

	public static IrcCommand? FromArgs(IList<string> args)
	{
		return args.Count switch
		{
			1 => new SAJOIN(args[0].Split(',')),
			2 => new SAJOIN(args[1].Split(','), args[0]),
			_ => null,
		};
	}
}