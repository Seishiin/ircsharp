using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using IRCSharp.Messaging;
using IRCSharp.Modes;
using Xunit;

namespace IRCSharp.Tests;

public class CommandTests
{
	private record CommandTest(string?[]? Args) : IrcCommand, IIrcCommand
	{
		public override string ToCommandString() => Stringify<CommandTest>(Args);
		public static string ToCommandStringNoArgs() => Stringify<CommandTest>();
		public static string Identifier() => "TEST";
		public static IrcCommand FromArgs(IList<string> args) => new CommandTest(args.ToArray());
	}

	[Fact]
	public void CommandStringifyWithoutArguments_ShouldStringifyProperly()
	{
		string expected = CommandTest.Identifier();
		string result = CommandTest.ToCommandStringNoArgs();
		Assert.Equal(expected, result);
	}

	[Theory]
	[MemberData(nameof(TestDataCommandStringify))]
	public void CommandStringifyWithArguments_ShouldStringifyProperly(string expected, string?[]? input)
	{
		var cmd = new CommandTest(input);
		string result = cmd.ToCommandString();
		Assert.Equal(expected, result);
	}

	public static IEnumerable<object?[]> TestDataCommandStringify()
	{
		string id = CommandTest.Identifier();
		yield return new object?[] { $"{id}", Array.Empty<object>() };
		yield return new object?[] { $"{id} :", null };
		yield return new object?[] { $"{id} :", new[] { "" } };
		yield return new object?[] { $"{id} foo", new[] { "foo" } };
		yield return new object?[] { $"{id} ::foo", new[] { ":foo" } };
		yield return new object?[] { $"{id} :foo bar", new[] { "foo bar" } };
		yield return new object?[] { $"{id} ::foo bar", new[] { ":foo bar" } };
		yield return new object?[] { $"{id} foo bar", new[] { "foo", "bar" } };
		yield return new object?[] { $"{id} foo ::bar", new[] { "foo", ":bar" } };
		yield return new object?[] { $"{id} foo :bar baz", new[] { "foo", "bar baz" } };
		yield return new object?[] { $"{id} foo ::bar baz", new[] { "foo", ":bar baz" } };
		yield return new object?[] { $"{id} foo", new[] { null, "foo" } };
		yield return new object?[] { $"{id} foo", new[] { "foo", null } };
		yield return new object?[] { $"{id} foo", new[] { null, null, null, "foo" } };
		yield return new object?[] { $"{id} :bar baz", new[] { null, "bar baz" } };
		yield return new object?[] { $"{id} :bar baz", new[] { "bar baz", null } };
		yield return new object?[] { $"{id} ::bar baz", new[] { null, ":bar baz" } };
		yield return new object?[] { $"{id} ::bar baz", new[] { ":bar baz", null } };
	}

	[Fact]
	public void PASSFromArgs_ShouldCreatePASS()
	{
		var expected = new IrcCommand.PASS("test_password");
		string[] args = { "test_password" };

		var result = IrcCommand.PASS.FromArgs(args);

		Assert.IsType<IrcCommand.PASS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NICKFromArgs_ShouldCreateNICK()
	{
		var expected = new IrcCommand.NICK("test_nickname");
		string[] args = { "test_nickname" };

		var result = IrcCommand.NICK.FromArgs(args);

		Assert.IsType<IrcCommand.NICK>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void USERFromArgs_ShouldCreateUSER()
	{
		var expected = new IrcCommand.USER("test_user", 0, "test_realname");
		string[] args = { "test_user", "0", "*", "test_realname" };

		var result = IrcCommand.USER.FromArgs(args);

		Assert.IsType<IrcCommand.USER>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void OPERFromArgs_ShouldCreateOPER()
	{
		var expected = new IrcCommand.OPER("test_name", "test_password");
		string[] args = { "test_name", "test_password" };

		var result = IrcCommand.OPER.FromArgs(args);

		Assert.IsType<IrcCommand.OPER>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_USERFromArgs_ShouldCreateMODE_USER()
	{
		var expected = new IrcCommand.MODE_USER("test_nickname");
		string[] args = { "test_nickname" };

		var result = IrcCommand.MODE_USER.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_USER>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_USERFromArgs_ShouldCreateMODE_USERWithModes()
	{
		var expected = new IrcCommand.MODE_USER("test_nickname", new IrcModes(new IrcMode(IrcModeSign.Minus, 'i')));
		string[] args = { "test_nickname", "-i" };

		var result = IrcCommand.MODE_USER.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_USER>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_USERFromArgs_ShouldCreateMODE_CHANNEL()
	{
		var expected = new IrcCommand.MODE_CHANNEL("#test_channel");
		string[] args = { "#test_channel" };

		var result = IrcCommand.MODE_USER.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_CHANNEL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_USERFromArgs_ShouldCreateMODE_CHANNELWithModes()
	{
		var expected = new IrcCommand.MODE_CHANNEL("#test_channel", new IrcModes(new IrcMode('k')));
		string[] args = { "#test_channel", "+k" };

		var result = IrcCommand.MODE_USER.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_CHANNEL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_USERFromArgs_ShouldCreateMODE_CHANNELWithModesAndModeArgs()
	{
		var expected = new IrcCommand.MODE_CHANNEL("#test_channel", new IrcModes(new IrcMode('k')), new[] { "chan_key" });
		string[] args = { "#test_channel", "+k", "chan_key" };

		var result = IrcCommand.MODE_USER.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_CHANNEL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SERVICEFromArgs_ShouldCreateSERVICE()
	{
		var expected = new IrcCommand.SERVICE("test_nickname", "test_distribution", "test_type", "test_info");
		string[] args = { "test_nickname", "*", "test_distribution", "test_type", "0", "test_info" };

		var result = IrcCommand.SERVICE.FromArgs(args);

		Assert.IsType<IrcCommand.SERVICE>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void QUITFromArgs_ShouldCreateQUIT()
	{
		var expected = new IrcCommand.QUIT();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.QUIT.FromArgs(args);

		Assert.IsType<IrcCommand.QUIT>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void QUITFromArgs_ShouldCreateQUITWithComment()
	{
		var expected = new IrcCommand.QUIT("test_comment");
		string[] args = { "test_comment" };

		var result = IrcCommand.QUIT.FromArgs(args);

		Assert.IsType<IrcCommand.QUIT>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SQUITFromArgs_ShouldCreateSQUIT()
	{
		var expected = new IrcCommand.SQUIT("test_server", "test_comment");
		string[] args = { "test_server", "test_comment" };

		var result = IrcCommand.SQUIT.FromArgs(args);

		Assert.IsType<IrcCommand.SQUIT>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void JOINFromArgs_ShouldCreateJOIN()
	{
		var expected = new IrcCommand.JOIN(new[] { "#test_channel1", "&test_channel2" });
		string[] args = { "#test_channel1,&test_channel2" };

		var result = IrcCommand.JOIN.FromArgs(args);

		Assert.IsType<IrcCommand.JOIN>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void JOINFromArgs_ShouldCreateJOINWithKeys()
	{
		var expected = new IrcCommand.JOIN(new[] { "#test_channel" }, new[] { "chan_key" });
		string[] args = { "#test_channel", "chan_key" };

		var result = IrcCommand.JOIN.FromArgs(args);

		Assert.IsType<IrcCommand.JOIN>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void JOINFromArgs_ShouldCreateJOINLeaveAll()
	{
		var expected = new IrcCommand.JOIN(Array.Empty<string>(), leaveAll: true);
		string[] args = { "0" };

		var result = IrcCommand.JOIN.FromArgs(args);

		Assert.IsType<IrcCommand.JOIN>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PARTFromArgs_ShouldCreatePART()
	{
		var expected = new IrcCommand.PART(new[] { "#test_channel1", "&test_channel2" });
		string[] args = { "#test_channel1,&test_channel2" };

		var result = IrcCommand.PART.FromArgs(args);

		Assert.IsType<IrcCommand.PART>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PARTFromArgs_ShouldCreatePARTWithComment()
	{
		var expected = new IrcCommand.PART(new[] { "#test_channel" }, "test_comment");
		string[] args = { "#test_channel", "test_comment" };

		var result = IrcCommand.PART.FromArgs(args);

		Assert.IsType<IrcCommand.PART>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_CHANNELFromArgs_ShouldCreateMODE_CHANNEL()
	{
		var expected = new IrcCommand.MODE_CHANNEL("#test_channel");
		string[] args = { "#test_channel" };

		var result = IrcCommand.MODE_CHANNEL.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_CHANNEL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_CHANNELFromArgs_ShouldCreateMODE_CHANNELWithModes()
	{
		var expected = new IrcCommand.MODE_CHANNEL("#test_channel", new IrcModes(new IrcMode('k')));
		string[] args = { "#test_channel", "+k" };

		var result = IrcCommand.MODE_CHANNEL.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_CHANNEL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_CHANNELFromArgs_ShouldCreateMODE_CHANNELWithModesAndModeArgs()
	{
		var expected = new IrcCommand.MODE_CHANNEL("#test_channel", new IrcModes(new IrcMode('k')), new[] { "chan_key" });
		string[] args = { "#test_channel", "+k", "chan_key" };

		var result = IrcCommand.MODE_CHANNEL.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_CHANNEL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_CHANNELFromArgs_ShouldCreateMODE_USER()
	{
		var expected = new IrcCommand.MODE_USER("test_nickname", new IrcModes(new IrcMode(IrcModeSign.Minus, 'i')));
		string[] args = { "test_nickname", "-i" };

		var result = IrcCommand.MODE_CHANNEL.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_USER>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MODE_CHANNELFromArgs_ShouldCreateMODE_USERWithModes()
	{
		var expected = new IrcCommand.MODE_USER("test_nickname");
		string[] args = { "test_nickname" };

		var result = IrcCommand.MODE_CHANNEL.FromArgs(args);

		Assert.IsType<IrcCommand.MODE_USER>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void TOPICFromArgs_ShouldCreateTOPIC()
	{
		var expected = new IrcCommand.TOPIC("#test_channel");
		string[] args = { "#test_channel" };

		var result = IrcCommand.TOPIC.FromArgs(args);

		Assert.IsType<IrcCommand.TOPIC>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void TOPICFromArgs_ShouldCreateTOPICWithTopic()
	{
		var expected = new IrcCommand.TOPIC("#test_channel", "test_topic");
		string[] args = { "#test_channel", "test_topic" };

		var result = IrcCommand.TOPIC.FromArgs(args);

		Assert.IsType<IrcCommand.TOPIC>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NAMESFromArgs_ShouldCreateNAMES()
	{
		var expected = new IrcCommand.NAMES();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.NAMES.FromArgs(args);

		Assert.IsType<IrcCommand.NAMES>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NAMESFromArgs_ShouldCreateNAMESWithChannels()
	{
		var expected = new IrcCommand.NAMES(new[] { "#test_channel1", "&test_channel2" });
		string[] args = { "#test_channel1,&test_channel2" };

		var result = IrcCommand.NAMES.FromArgs(args);

		Assert.IsType<IrcCommand.NAMES>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NAMESFromArgs_ShouldCreateNAMESWithChannelsAndServer()
	{
		var expected = new IrcCommand.NAMES(new[] { "#test_channel" }, "test_server");
		string[] args = { "#test_channel", "test_server" };

		var result = IrcCommand.NAMES.FromArgs(args);

		Assert.IsType<IrcCommand.NAMES>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LISTFromArgs_ShouldCreateLIST()
	{
		var expected = new IrcCommand.LIST();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.LIST.FromArgs(args);

		Assert.IsType<IrcCommand.LIST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LISTFromArgs_ShouldCreateLISTWithChannels()
	{
		var expected = new IrcCommand.LIST(new[] { "#test_channel1", "&test_channel2" });
		string[] args = { "#test_channel1,&test_channel2" };

		var result = IrcCommand.LIST.FromArgs(args);

		Assert.IsType<IrcCommand.LIST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LISTFromArgs_ShouldCreateLISTWithChannelsAndServer()
	{
		var expected = new IrcCommand.LIST(new[] { "#test_channel" }, "test_server");
		string[] args = { "#test_channel", "test_server" };

		var result = IrcCommand.LIST.FromArgs(args);

		Assert.IsType<IrcCommand.LIST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void INVITEFromArgs_ShouldCreateINVITE()
	{
		var expected = new IrcCommand.INVITE("test_nickname", "#test_channel");
		string[] args = { "test_nickname", "#test_channel" };

		var result = IrcCommand.INVITE.FromArgs(args);

		Assert.IsType<IrcCommand.INVITE>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void KICKFromArgs_ShouldCreateKICK()
	{
		var expected = new IrcCommand.KICK(new[] { "#test_channel1", "&test_channel2" }, new[] { "test_user" });
		string[] args = { "#test_channel1,&test_channel2", "test_user" };

		var result = IrcCommand.KICK.FromArgs(args);

		Assert.IsType<IrcCommand.KICK>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void KICKFromArgs_ShouldCreateKICKWithComment()
	{
		var expected = new IrcCommand.KICK(new[] { "#test_channel" }, new[] { "test_user" }, "test_comment");
		string[] args = { "#test_channel", "test_user", "test_comment" };

		var result = IrcCommand.KICK.FromArgs(args);

		Assert.IsType<IrcCommand.KICK>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PRIVMSGFromArgs_ShouldCreatePRIVMSG()
	{
		var expected = new IrcCommand.PRIVMSG("test_target", "test_text");
		string[] args = { "test_target", "test_text" };

		var result = IrcCommand.PRIVMSG.FromArgs(args);

		Assert.IsType<IrcCommand.PRIVMSG>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PRIVMSGFromArgs_ShouldCreateCTCP()
	{
		var expected = new IrcCommand.CTCP(CtcpType.Query, "test_target", "VERSION");
		string[] args = { "test_target", $"{'\x01'}VERSION{'\x01'}" };

		var result = IrcCommand.PRIVMSG.FromArgs(args);

		Assert.IsType<IrcCommand.CTCP>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PRIVMSGFromArgs_ShouldCreateCTCPWithText()
	{
		var expected = new IrcCommand.CTCP(CtcpType.Query, "test_target", "DCC", "SEND \"filename with spaces\" 123 456 789");
		string[] args = { "test_target", $"{'\x01'}DCC SEND \"filename with spaces\" 123 456 789{'\x01'}" };

		var result = IrcCommand.PRIVMSG.FromArgs(args);

		Assert.IsType<IrcCommand.CTCP>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NOTICEFromArgs_ShouldCreateNOTICE()
	{
		var expected = new IrcCommand.NOTICE("test_target", "test_text");
		string[] args = { "test_target", "test_text" };

		var result = IrcCommand.NOTICE.FromArgs(args);

		Assert.IsType<IrcCommand.NOTICE>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NOTICEFromArgs_ShouldCreateCTCP()
	{
		var expected = new IrcCommand.CTCP(CtcpType.Reply, "test_target", "VERSION");
		string[] args = { "test_target", $"{'\x01'}VERSION{'\x01'}" };

		var result = IrcCommand.NOTICE.FromArgs(args);

		Assert.IsType<IrcCommand.CTCP>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void NOTICEFromArgs_ShouldCreateCTCPWithText()
	{
		var expected = new IrcCommand.CTCP(CtcpType.Reply, "test_target", "DCC", "ACCEPT \"filename with spaces\" 123 456");
		string[] args = { "test_target", $"{'\x01'}DCC ACCEPT \"filename with spaces\" 123 456{'\x01'}" };

		var result = IrcCommand.NOTICE.FromArgs(args);

		Assert.IsType<IrcCommand.CTCP>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MOTDFromArgs_ShouldCreateMOTD()
	{
		var expected = new IrcCommand.MOTD();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.MOTD.FromArgs(args);

		Assert.IsType<IrcCommand.MOTD>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MOTDFromArgs_ShouldCreateMOTDWithServer()
	{
		var expected = new IrcCommand.MOTD("test_server");
		string[] args = { "test_server" };

		var result = IrcCommand.MOTD.FromArgs(args);

		Assert.IsType<IrcCommand.MOTD>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LUSERSFromArgs_ShouldCreateLUSERS()
	{
		var expected = new IrcCommand.LUSERS();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.LUSERS.FromArgs(args);

		Assert.IsType<IrcCommand.LUSERS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LUSERSFromArgs_ShouldCreateLUSERSWithMask()
	{
		var expected = new IrcCommand.LUSERS("test_mask");
		string[] args = { "test_mask" };

		var result = IrcCommand.LUSERS.FromArgs(args);

		Assert.IsType<IrcCommand.LUSERS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LUSERSFromArgs_ShouldCreateLUSERSWithMaskAndServer()
	{
		var expected = new IrcCommand.LUSERS("test_mask", "test_server");
		string[] args = { "test_mask", "test_server" };

		var result = IrcCommand.LUSERS.FromArgs(args);

		Assert.IsType<IrcCommand.LUSERS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void VERSIONFromArgs_ShouldCreateVERSION()
	{
		var expected = new IrcCommand.VERSION();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.VERSION.FromArgs(args);

		Assert.IsType<IrcCommand.VERSION>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void VERSIONFromArgs_ShouldCreateVERSIONWithTarget()
	{
		var expected = new IrcCommand.VERSION("test_target");
		string[] args = { "test_target" };

		var result = IrcCommand.VERSION.FromArgs(args);

		Assert.IsType<IrcCommand.VERSION>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void STATSFromArgs_ShouldCreateSTATS()
	{
		var expected = new IrcCommand.STATS();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.STATS.FromArgs(args);

		Assert.IsType<IrcCommand.STATS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void STATSFromArgs_ShouldCreateSTATSWithQuery()
	{
		var expected = new IrcCommand.STATS("test_query");
		string[] args = { "test_query" };

		var result = IrcCommand.STATS.FromArgs(args);

		Assert.IsType<IrcCommand.STATS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void STATSFromArgs_ShouldCreateSTATSWithQueryAndServer()
	{
		var expected = new IrcCommand.STATS("test_query", "test_server");
		string[] args = { "test_query", "test_server" };

		var result = IrcCommand.STATS.FromArgs(args);

		Assert.IsType<IrcCommand.STATS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LINKSFromArgs_ShouldCreateLINKS()
	{
		var expected = new IrcCommand.LINKS();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.LINKS.FromArgs(args);

		Assert.IsType<IrcCommand.LINKS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LINKSFromArgs_ShouldCreateLINKSWithRemoteServer()
	{
		var expected = new IrcCommand.LINKS("test_remoteserver");
		string[] args = { "test_remoteserver" };

		var result = IrcCommand.LINKS.FromArgs(args);

		Assert.IsType<IrcCommand.LINKS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void LINKSFromArgs_ShouldCreateLINKSWithRemoteServerAndServerMask()
	{
		var expected = new IrcCommand.LINKS("test_remoteserver", "test_servermask");
		string[] args = { "test_remoteserver", "test_servermask" };

		var result = IrcCommand.LINKS.FromArgs(args);

		Assert.IsType<IrcCommand.LINKS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void TIMEFromArgs_ShouldCreateTIME()
	{
		var expected = new IrcCommand.TIME();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.TIME.FromArgs(args);

		Assert.IsType<IrcCommand.TIME>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void TIMEFromArgs_ShouldCreateTIMEWithServer()
	{
		var expected = new IrcCommand.TIME("test_server");
		string[] args = { "test_server" };

		var result = IrcCommand.TIME.FromArgs(args);

		Assert.IsType<IrcCommand.TIME>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void CONNECTFromArgs_ShouldCreateCONNECT()
	{
		var expected = new IrcCommand.CONNECT("test_targetserver", "test_port");
		string[] args = { "test_targetserver", "test_port" };

		var result = IrcCommand.CONNECT.FromArgs(args);

		Assert.IsType<IrcCommand.CONNECT>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void CONNECTFromArgs_ShouldCreateCONNECTWithRemoteServer()
	{
		var expected = new IrcCommand.CONNECT("test_targetserver", "test_port", "test_remoteserver");
		string[] args = { "test_targetserver", "test_port", "test_remoteserver" };

		var result = IrcCommand.CONNECT.FromArgs(args);

		Assert.IsType<IrcCommand.CONNECT>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void TRACEFromArgs_ShouldCreateTRACE()
	{
		var expected = new IrcCommand.TRACE();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.TRACE.FromArgs(args);

		Assert.IsType<IrcCommand.TRACE>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void TRACEFromArgs_ShouldCreateTRACEWithTarget()
	{
		var expected = new IrcCommand.TRACE("test_target");
		string[] args = { "test_target" };

		var result = IrcCommand.TRACE.FromArgs(args);

		Assert.IsType<IrcCommand.TRACE>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ADMINFromArgs_ShouldCreateADMIN()
	{
		var expected = new IrcCommand.ADMIN();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.ADMIN.FromArgs(args);

		Assert.IsType<IrcCommand.ADMIN>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ADMINFromArgs_ShouldCreateADMINWithTarget()
	{
		var expected = new IrcCommand.ADMIN("test_target");
		string[] args = { "test_target" };

		var result = IrcCommand.ADMIN.FromArgs(args);

		Assert.IsType<IrcCommand.ADMIN>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void INFOFromArgs_ShouldCreateINFO()
	{
		var expected = new IrcCommand.INFO();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.INFO.FromArgs(args);

		Assert.IsType<IrcCommand.INFO>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void INFOFromArgs_ShouldCreateINFOWithServer()
	{
		var expected = new IrcCommand.INFO("test_server");
		string[] args = { "test_server" };

		var result = IrcCommand.INFO.FromArgs(args);

		Assert.IsType<IrcCommand.INFO>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void HELPFromArgs_ShouldCreateHELP()
	{
		var expected = new IrcCommand.HELP();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.HELP.FromArgs(args);

		Assert.IsType<IrcCommand.HELP>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void HELPFromArgs_ShouldCreateHELPWithSubject()
	{
		var expected = new IrcCommand.HELP("test_subject");
		string[] args = { "test_subject" };

		var result = IrcCommand.HELP.FromArgs(args);

		Assert.IsType<IrcCommand.HELP>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SERVLISTFromArgs_ShouldCreateSERVLIST()
	{
		var expected = new IrcCommand.SERVLIST();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.SERVLIST.FromArgs(args);

		Assert.IsType<IrcCommand.SERVLIST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SERVLISTFromArgs_ShouldCreateSERVLISTWithMask()
	{
		var expected = new IrcCommand.SERVLIST("test_mask");
		string[] args = { "test_mask" };

		var result = IrcCommand.SERVLIST.FromArgs(args);

		Assert.IsType<IrcCommand.SERVLIST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SERVLISTFromArgs_ShouldCreateSERVLISTWithMaskAndType()
	{
		var expected = new IrcCommand.SERVLIST("test_mask", "test_type");
		string[] args = { "test_mask", "test_type" };

		var result = IrcCommand.SERVLIST.FromArgs(args);

		Assert.IsType<IrcCommand.SERVLIST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SQUERYFromArgs_ShouldCreateSQUERY()
	{
		var expected = new IrcCommand.SQUERY("test_servicename", "test_text");
		string[] args = { "test_servicename", "test_text" };

		var result = IrcCommand.SQUERY.FromArgs(args);

		Assert.IsType<IrcCommand.SQUERY>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOFromArgs_ShouldCreateWHO()
	{
		var expected = new IrcCommand.WHO();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.WHO.FromArgs(args);

		Assert.IsType<IrcCommand.WHO>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOFromArgs_ShouldCreateWHOWithMask()
	{
		var expected = new IrcCommand.WHO("test_mask");
		string[] args = { "test_mask" };

		var result = IrcCommand.WHO.FromArgs(args);

		Assert.IsType<IrcCommand.WHO>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOFromArgs_ShouldCreateWHOWithMaskAndOps()
	{
		var expected = new IrcCommand.WHO("test_mask", true);
		string[] args = { "test_mask", "o" };

		var result = IrcCommand.WHO.FromArgs(args);

		Assert.IsType<IrcCommand.WHO>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOISFromArgs_ShouldCreateWHOIS()
	{
		var expected = new IrcCommand.WHOIS(new[] { "test_mask1", "test_mask2" });
		string[] args = { "test_mask1,test_mask2" };

		var result = IrcCommand.WHOIS.FromArgs(args);

		Assert.IsType<IrcCommand.WHOIS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOISFromArgs_ShouldCreateWHOISWithTarget()
	{
		var expected = new IrcCommand.WHOIS(new[] { "test_mask" }, "test_target");
		string[] args = { "test_target", "test_mask" };

		var result = IrcCommand.WHOIS.FromArgs(args);

		Assert.IsType<IrcCommand.WHOIS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOWASFromArgs_ShouldCreateWHOWAS()
	{
		var expected = new IrcCommand.WHOWAS(new[] { "test_nickname1", "test_nickname2" });
		string[] args = { "test_nickname1,test_nickname2" };

		var result = IrcCommand.WHOWAS.FromArgs(args);

		Assert.IsType<IrcCommand.WHOWAS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOWASFromArgs_ShouldCreateWHOWASWithCount()
	{
		var expected = new IrcCommand.WHOWAS(new[] { "test_nickname" }, 5);
		string[] args = { "test_nickname", "5" };

		var result = IrcCommand.WHOWAS.FromArgs(args);

		Assert.IsType<IrcCommand.WHOWAS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WHOWASFromArgs_ShouldCreateWHOWASWithCountAndTarget()
	{
		var expected = new IrcCommand.WHOWAS(new[] { "test_nickname" }, 9, "test_target");
		string[] args = { "test_nickname", "9", "test_target" };

		var result = IrcCommand.WHOWAS.FromArgs(args);

		Assert.IsType<IrcCommand.WHOWAS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void KILLFromArgs_ShouldCreateKILL()
	{
		var expected = new IrcCommand.KILL("test_nickname", "test_comment");
		string[] args = { "test_nickname", "test_comment" };

		var result = IrcCommand.KILL.FromArgs(args);

		Assert.IsType<IrcCommand.KILL>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PINGFromArgs_ShouldCreatePING()
	{
		var expected = new IrcCommand.PING("test_server1");
		string[] args = { "test_server1" };

		var result = IrcCommand.PING.FromArgs(args);

		Assert.IsType<IrcCommand.PING>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PINGFromArgs_ShouldCreatePINGWithServer2()
	{
		var expected = new IrcCommand.PING("test_server1", "test_server2");
		string[] args = { "test_server1", "test_server2" };

		var result = IrcCommand.PING.FromArgs(args);

		Assert.IsType<IrcCommand.PING>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PONGFromArgs_ShouldCreatePONG()
	{
		var expected = new IrcCommand.PONG("test_server");
		string[] args = { "test_server" };

		var result = IrcCommand.PONG.FromArgs(args);

		Assert.IsType<IrcCommand.PONG>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void PONGFromArgs_ShouldCreatePONGWithServer2()
	{
		var expected = new IrcCommand.PONG("test_server", "test_server2");
		string[] args = { "test_server", "test_server2" };

		var result = IrcCommand.PONG.FromArgs(args);

		Assert.IsType<IrcCommand.PONG>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ERRORFromArgs_ShouldCreateERROR()
	{
		var expected = new IrcCommand.ERROR("test_message");
		string[] args = { "test_message" };

		var result = IrcCommand.ERROR.FromArgs(args);

		Assert.IsType<IrcCommand.ERROR>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void AWAYFromArgs_ShouldCreateAWAY()
	{
		var expected = new IrcCommand.AWAY();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.AWAY.FromArgs(args);

		Assert.IsType<IrcCommand.AWAY>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void AWAYFromArgs_ShouldCreateAWAYWithComment()
	{
		var expected = new IrcCommand.AWAY("test_comment");
		string[] args = { "test_comment" };

		var result = IrcCommand.AWAY.FromArgs(args);

		Assert.IsType<IrcCommand.AWAY>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void REHASHFromArgs_ShouldCreateREHASH()
	{
		var expected = new IrcCommand.REHASH();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.REHASH.FromArgs(args);

		Assert.IsType<IrcCommand.REHASH>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void DIEFromArgs_ShouldCreateDIE()
	{
		var expected = new IrcCommand.DIE();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.DIE.FromArgs(args);

		Assert.IsType<IrcCommand.DIE>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void RESTARTFromArgs_ShouldCreateRESTART()
	{
		var expected = new IrcCommand.RESTART();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.RESTART.FromArgs(args);

		Assert.IsType<IrcCommand.RESTART>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SUMMONFromArgs_ShouldCreateSUMMON()
	{
		var expected = new IrcCommand.SUMMON("test_user");
		string[] args = { "test_user" };

		var result = IrcCommand.SUMMON.FromArgs(args);

		Assert.IsType<IrcCommand.SUMMON>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SUMMONFromArgs_ShouldCreateSUMMONWithServer()
	{
		var expected = new IrcCommand.SUMMON("test_user", "test_server");
		string[] args = { "test_user", "test_server" };

		var result = IrcCommand.SUMMON.FromArgs(args);

		Assert.IsType<IrcCommand.SUMMON>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void SUMMONFromArgs_ShouldCreateSUMMONWithServerAndChannel()
	{
		var expected = new IrcCommand.SUMMON("test_user", "test_server", "#test_channel");
		string[] args = { "test_user", "test_server", "#test_channel" };

		var result = IrcCommand.SUMMON.FromArgs(args);

		Assert.IsType<IrcCommand.SUMMON>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void USERSFromArgs_ShouldCreateUSERS()
	{
		var expected = new IrcCommand.USERS();
		string[] args = Array.Empty<string>();

		var result = IrcCommand.USERS.FromArgs(args);

		Assert.IsType<IrcCommand.USERS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void USERSFromArgs_ShouldCreateUSERSWithServer()
	{
		var expected = new IrcCommand.USERS("test_server");
		string[] args = { "test_server" };

		var result = IrcCommand.USERS.FromArgs(args);

		Assert.IsType<IrcCommand.USERS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void WALLOPSFromArgs_ShouldCreateWALLOPS()
	{
		var expected = new IrcCommand.WALLOPS("test_text");
		string[] args = { "test_text" };

		var result = IrcCommand.WALLOPS.FromArgs(args);

		Assert.IsType<IrcCommand.WALLOPS>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void USERHOSTFromArgs_ShouldCreateUSERHOST()
	{
		var expected = new IrcCommand.USERHOST(new[] { "test_nickname1", "test_nickname2" });
		string[] args = { "test_nickname1", "test_nickname2" };

		var result = IrcCommand.USERHOST.FromArgs(args);

		Assert.IsType<IrcCommand.USERHOST>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ISONFromArgs_ShouldCreateISON()
	{
		var expected = new IrcCommand.ISON(new[] { "test_nickname1", "test_nickname2" });
		string[] args = { "test_nickname1", "test_nickname2" };

		var result = IrcCommand.ISON.FromArgs(args);

		Assert.IsType<IrcCommand.ISON>(result);
		Assert.Equivalent(expected, result, true);
	}

	[Theory]
	[MemberData(nameof(TestDataCommandFromArgs))]
	public void CommandFromArgs_ShouldReturnNullWhenIncorrectNumberOfArgs(Type type, IList<string> input)
	{
		const string methodName = nameof(IIrcCommand.FromArgs);
		var fromArgs = type.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static);

		if (fromArgs == null)
			throw new InvalidOperationException($"{type} does not have a {methodName} method");

		object? result = fromArgs.Invoke(null, new[] { (object)input });
		Assert.Null(result);
	}

	public static IEnumerable<object[]> TestDataCommandFromArgs()
	{
		yield return new object[] { typeof(IrcCommand.PASS), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.PASS), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.NICK), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.NICK), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.USER), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.USER), new[] { "a", "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.OPER), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.OPER), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.MODE_USER), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.MODE_USER), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.SERVICE), new[] { "a", "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.SERVICE), new[] { "a", "a", "a", "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.QUIT), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.SQUIT), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.SQUIT), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.JOIN), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.JOIN), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.PART), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.PART), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.MODE_CHANNEL), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.TOPIC), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.TOPIC), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.NAMES), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.LIST), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.INVITE), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.INVITE), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.KICK), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.KICK), new[] { "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.PRIVMSG), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.PRIVMSG), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.NOTICE), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.NOTICE), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.MOTD), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.LUSERS), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.VERSION), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.STATS), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.LINKS), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.TIME), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.CONNECT), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.CONNECT), new[] { "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.TRACE), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.ADMIN), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.INFO), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.HELP), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.SERVLIST), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.SQUERY), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.SQUERY), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.WHO), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.WHOIS), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.WHOIS), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.WHOWAS), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.WHOWAS), new[] { "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.KILL), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.KILL), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.PING), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.PING), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.PONG), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.PONG), new[] { "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.ERROR), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.ERROR), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.AWAY), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.REHASH), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.DIE), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.RESTART), new[] { "a" } };
		yield return new object[] { typeof(IrcCommand.SUMMON), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.SUMMON), new[] { "a", "a", "a", "a" } };
		yield return new object[] { typeof(IrcCommand.USERS), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.WALLOPS), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.WALLOPS), new[] { "a", "a" } };
		yield return new object[] { typeof(IrcCommand.USERHOST), Array.Empty<string>() };
		yield return new object[] { typeof(IrcCommand.ISON), Array.Empty<string>() };
	}
}