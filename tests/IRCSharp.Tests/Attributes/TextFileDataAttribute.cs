using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Xunit.Sdk;

namespace IRCSharp.Tests.Attributes;

public class TextFileDataAttribute : DataAttribute
{
	private readonly string filePath;

	public TextFileDataAttribute(string? file)
	{
		ArgumentNullException.ThrowIfNull(file);

		filePath = Path.GetFullPath(file);

		if (!File.Exists(filePath))
			throw new FileNotFoundException(filePath);
	}

	public override IEnumerable<object[]> GetData(MethodInfo testMethod)
	{
		ArgumentNullException.ThrowIfNull(testMethod);

		return File.ReadAllLines(filePath).Select(line => new[] { line });
	}
}