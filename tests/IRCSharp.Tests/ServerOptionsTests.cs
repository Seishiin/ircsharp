using System.Collections.Generic;
using IRCSharp.Messaging;
using IRCSharp.Modes;
using Xunit;

namespace IRCSharp.Tests;

public class ServerOptionsTests
{
	[Fact]
	public void ServerOptionsParse_ShouldAddSpecifiedArguments()
	{
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("CALLERID", "KICKLEN=180", "STATUSMSG=~&@%+");

		sut.ParseISupport(cmd);

		Assert.False(sut.Has("test_user"));
		Assert.True(sut.Has("CALLERID"));
		Assert.True(sut.Has("KICKLEN"));
		Assert.True(sut.Has("STATUSMSG"));
		Assert.False(sut.Has("are supported by this server"));
	}

	[Fact]
	public void ServerOptionsParse_ShouldRemoveMinusPrefixedArgument()
	{
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("CALLERID", "-CALLERID");

		sut.ParseISupport(cmd);

		Assert.False(sut.Has("CALLERID"));
	}

	[Fact]
	public void ServerOptionsParse_ShouldUpdateSpecifiedArgument()
	{
		const int expected = 30;
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("NICKLEN=10", $"NICKLEN={expected}");

		sut.ParseISupport(cmd);
		int result = sut.NICKLEN();

		Assert.Equal(expected, result);
	}

	[Fact]
	public void ServerOptionsParameter_ShouldReturnSpecifiedInt()
	{
		const int expected = 25;
		var sut = new IrcServerOptions();
		var cmd = CreateResponse($"NICKLEN={expected}");

		sut.ParseISupport(cmd);
		int result = sut.NICKLEN();

		Assert.Equal(expected, result);
	}

	[Fact]
	public void ServerOptionsParameter_ShouldReturnDefaultInt()
	{
		const int expected = 9;
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("NICKLEN=");

		sut.ParseISupport(cmd);
		int result = sut.NICKLEN();

		Assert.Equal(expected, result);
	}

	[Fact]
	public void ServerOptionsParameter_ShouldReturnSpecifiedChar()
	{
		const char expected = 'f';
		var sut = new IrcServerOptions();
		var cmd = CreateResponse($"EXCEPTS={expected}");

		sut.ParseISupport(cmd);
		char result = sut.EXCEPTS();

		Assert.Equal(expected, result);
	}

	[Fact]
	public void ServerOptionsParameter_ShouldReturnDefaultChar()
	{
		const char expected = 'e';
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("EXCEPTS=");

		sut.ParseISupport(cmd);
		char result = sut.EXCEPTS();

		Assert.Equal(expected, result);
	}

	[Fact]
	public void ServerOptionsParameter_ShouldReturnSpecifiedCharArray()
	{
		char[] expected = { '#', '&', '+' };
		var sut = new IrcServerOptions();
		var cmd = CreateResponse($"CHANTYPES={string.Concat(expected)}");

		sut.ParseISupport(cmd);
		char[] result = sut.CHANTYPES();

		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ServerOptionsParameter_ShouldReturnEmptyCharArray()
	{
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("CHANTYPES=");

		sut.ParseISupport(cmd);
		char[] result = sut.CHANTYPES();

		Assert.Empty(result);
	}

	[Fact]
	public void ServerOptionsTARGMAX_ShouldParse()
	{
		var expected = new Dictionary<string, int>
		{
			["ACCEPT"] = -1,
			["KICK"] = 1,
			["PRIVMSG"] = 4,
			["WHOIS"] = 2,
		};
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("TARGMAX=ACCEPT:,KICK:1,PRIVMSG:4,whois:2");

		sut.ParseISupport(cmd);
		var result = sut.TARGMAX();

		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ServerOptionsCHANLIMIT_ShouldParse()
	{
		var expected = new Dictionary<char, int>
		{
			['#'] = 70,
			['+'] = 70,
			['&'] = -1,
		};
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("CHANLIMIT=#+:70,&:");

		sut.ParseISupport(cmd);
		var result = sut.CHANLIMIT();

		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ServerOptionsMAXLIST_ShouldParse()
	{
		var expected = new Dictionary<char, int>
		{
			['b'] = 100,
			['e'] = 100,
			['I'] = 100,
			['q'] = 50,
		};
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("MAXLIST=beI:100,q:50,s:,f");

		sut.ParseISupport(cmd);
		var result = sut.MAXLIST();

		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ServerOptionsIDCHAN_ShouldParse()
	{
		var expected = new Dictionary<char, int>
		{
			['!'] = 5,
			['#'] = 3,
		};
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("IDCHAN=!:5,#:3,+:,&");

		sut.ParseISupport(cmd);
		var result = sut.IDCHAN();

		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void ServerOptionsPREFIX_ShouldParse()
	{
		var expected = new Dictionary<char, char>
		{
			['q'] = '~',
			['a'] = '&',
			['o'] = '@',
			['h'] = '%',
			['v'] = '+',
		};
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("PREFIX=(qaohv)~&@%+");

		sut.ParseISupport(cmd);
		var result = sut.PREFIX();

		Assert.Equivalent(expected, result, true);
	}

	[Theory]
	[InlineData("qaohv)~&@%+")]
	[InlineData("(qaohv~&@%+")]
	[InlineData("(qao)~&@%+")]
	[InlineData("(qaohv)~%+")]
	public void ServerOptionsPREFIX_ShouldReturnEmptyDictionary(string input)
	{
		var sut = new IrcServerOptions();
		var cmd = CreateResponse($"PREFIX={input}");

		sut.ParseISupport(cmd);
		var result = sut.PREFIX();

		Assert.Empty(result);
	}

	[Fact]
	public void ServerOptionsCHANMODES_ShouldParse()
	{
		var expected = new IrcModeTypes
		{
			A = new[] { 'b', 'e', 'I' },
			B = new[] { 'k' },
			C = new[] { 'l' },
			D = new[] { 'B', 'C', 'M', 'N' },
		};
		var sut = new IrcServerOptions();
		var cmd = CreateResponse("CHANMODES=beI,k,l,BCMN");

		sut.ParseISupport(cmd);
		var result = sut.CHANMODES();

		Assert.Equivalent(expected, result, true);
	}

	private static IrcCommand.RESPONSE CreateResponse(params string[] vals)
	{
		var args = new List<string> { "test_user" };
		args.AddRange(vals);
		args.Add("are supported by this server");
		return new IrcCommand.RESPONSE(IrcNumericReply.RPL_ISUPPORT, args);
	}
}