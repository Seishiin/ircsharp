using System.Collections.Generic;
using Xunit;

namespace IRCSharp.Tests;

public class FormattingParserTests
{
	[Theory]
	[MemberData(nameof(TestDataPlainText))]
	public void IsFormatted_ShouldReturnFalse(string input)
	{
		Assert.False(input.IsFormatted());
	}

	[Theory]
	[MemberData(nameof(TestDataIsFormatted))]
	public void IsFormatted_ShouldReturnTrue(string input)
	{
		Assert.True(input.IsFormatted());
	}

	[Theory]
	[MemberData(nameof(TestDataPlainText))]
	public void StripFormatting_ShouldDoNothing(string input)
	{
		Assert.Equal(input, input.StripFormatting());
	}

	[Theory]
	[MemberData(nameof(TestDataStripFormatting))]
	public void StripFormatting_ShouldRemoveFormatting(string expected, string input)
	{
		Assert.Equal(expected, input.StripFormatting());
	}

	public static IEnumerable<object[]> TestDataPlainText()
	{
		yield return new object[] { "" };
		yield return new object[] { "    " };
		yield return new object[] { "😃..." };
		yield return new object[] { "a plain text" };
		yield return new object[] { "\t\r\n" };
	}

	public static IEnumerable<object[]> TestDataIsFormatted()
	{
		yield return new object[] { $"formatted {'\x02'} text" };
		yield return new object[] { $"formatted {'\x1D'} text" };
		yield return new object[] { $"formatted {'\x1F'} text" };
		yield return new object[] { $"formatted {'\x1E'} text" };
		yield return new object[] { $"formatted {'\x11'} text" };
		yield return new object[] { $"formatted {'\x16'} text" };
		yield return new object[] { $"formatted {'\x0F'} text" };
		yield return new object[] { $"formatted {'\x03'} text" };
		yield return new object[] { $"formatted {'\x04'} text" };
	}

	public static IEnumerable<object[]> TestDataStripFormatting()
	{
		yield return new object[] { "test", $"t{'\x02'}est" };
		yield return new object[] { "우왕굳", "우왕\x02굳" };
		yield return new object[] { "test", $"te{'\x03'}st" };
		yield return new object[] { "te,st", $"te{'\x03'},st" };
		yield return new object[] { "test", $"te{'\x03'}0,st" };
		yield return new object[] { "test", $"te{'\x03'}12,st" };
		yield return new object[] { "test", $"test{'\x03'}1" };
		yield return new object[] { "test", $"test{'\x03'}12" };
		yield return new object[] { "test", $"test{'\x03'}12," };
		yield return new object[] { "test", $"test{'\x03'}12,1" };
		yield return new object[] { "test", $"test{'\x03'}12,13" };
		yield return new object[] { "test,", $"test{'\x03'}12,13," };
		yield return new object[] { "test", $"te{'\x03'}12,1st" };
		yield return new object[] { "test", $"te{'\x03'}12,13st" };
		yield return new object[] { "test", $"te{'\x03'}3st" };
		yield return new object[] { "test", $"te{'\x03'}12st" };
		yield return new object[] { "test", $"te{'\x03'}1,2st" };
		yield return new object[] { "test", $"te{'\x03'}12,3st" };
		yield return new object[] { "test", $"te{'\x03'}1,12st" };
		yield return new object[] { "test", $"te{'\x03'}12,13st" };
		yield return new object[] { "foobar", $"foo{'\x03'}4b{'\x03'}3a{'\x03'}12r" };
		yield return new object[] { "", $"{'\x03'}44{'\x03'}55{'\x03'}66" };
		yield return new object[] { "foobar", $"foo{'\x03'}10b{'\x03'}11a{'\x03'}12r" };
		yield return new object[] { "121110", $"{'\x03'}1212{'\x03'}1111{'\x03'}1010" };
		yield return new object[] { "test", $"te{'\x04'}st" };
		yield return new object[] { "test", $"te{'\x04'}FFFFFFst" };
		yield return new object[] { "test", $"te{'\x04'}ffffffst" };
		yield return new object[] { "test", $"te{'\x04'}012345st" };
		yield return new object[] { "test", $"te{'\x04'}aa00aast" };
		yield return new object[] { "test", $"te{'\x04'}BB11BBst" };
		yield return new object[] { "test", $"te{'\x04'}00FFFFst" };
		yield return new object[] { "test", $"te{'\x04'}00FFFF,st" };
		yield return new object[] { "test", $"te{'\x04'}00FFFF,11AAAAst" };
	}
}