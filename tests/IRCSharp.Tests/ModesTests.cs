using System.Collections.Generic;
using IRCSharp.Modes;
using Xunit;

namespace IRCSharp.Tests;

public class ModesTests
{
	[Theory]
	[MemberData(nameof(TestDataModesToString))]
	public void ModesToString_ShouldStringifyProperly(string expected, IrcModes input)
	{
		Assert.Equal(expected, input.ToString());
	}

	[Theory]
	[MemberData(nameof(TestDataModesParse))]
	public void Modes_ShouldParse(IrcModes expected, string input)
	{
		var result = IrcModes.Parse(input);
		Assert.Equivalent(expected, result, true);
	}

	public static IEnumerable<object?[]> TestDataModesToString()
	{
		yield return new object?[]
		{
			"", new IrcModes(),
		};
		yield return new object?[]
		{
			"+i", new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i')
			),
		};
		yield return new object?[]
		{
			"-o", new IrcModes
			(
				new IrcMode(IrcModeSign.Minus, 'o')
			),
		};
		yield return new object?[]
		{
			"+io", new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i'),
				new IrcMode(IrcModeSign.Plus, 'o')
			),
		};
		yield return new object?[]
		{
			"+io-ws", new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i'),
				new IrcMode(IrcModeSign.Plus, 'o'),
				new IrcMode(IrcModeSign.Minus, 'w'),
				new IrcMode(IrcModeSign.Minus, 's')
			),
		};
	}

	public static IEnumerable<object?[]> TestDataModesParse()
	{
		yield return new object?[]
		{
			new IrcModes(), "",
		};
		yield return new object?[]
		{
			new IrcModes(), "i",
		};
		yield return new object?[]
		{
			new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i')
			),
			"+i",
		};
		yield return new object?[]
		{
			new IrcModes
			(
				new IrcMode(IrcModeSign.Minus, 'o')
			),
			"-o",
		};
		yield return new object?[]
		{
			new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i'),
				new IrcMode(IrcModeSign.Plus, 'o')
			),
			"+io",
		};
		yield return new object?[]
		{
			new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i'),
				new IrcMode(IrcModeSign.Plus, 'o')
			),
			"abc+io",
		};
		yield return new object?[]
		{
			new IrcModes
			(
				new IrcMode(IrcModeSign.Plus, 'i'),
				new IrcMode(IrcModeSign.Plus, 'o'),
				new IrcMode(IrcModeSign.Minus, 'w'),
				new IrcMode(IrcModeSign.Minus, 's')
			),
			"+io-ws",
		};
	}
}