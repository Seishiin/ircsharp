using System;
using System.Collections.Generic;
using IRCSharp.Messaging;
using IRCSharp.Parsing;
using IRCSharp.Tests.Attributes;
using Xunit;

namespace IRCSharp.Tests;

public class MessageParserTests
{
	private readonly MessageParser sut;

	public MessageParserTests()
	{
		var cmdMap = new Dictionary<string, Func<IList<string>, IrcCommand?>>(StringComparer.OrdinalIgnoreCase)
		{
			[IrcCommand.AWAY.Identifier()] = IrcCommand.AWAY.FromArgs,
			[IrcCommand.SQUIT.Identifier()] = IrcCommand.SQUIT.FromArgs,
			[IrcCommand.PRIVMSG.Identifier()] = IrcCommand.PRIVMSG.FromArgs,
			[IrcCommand.USER.Identifier()] = IrcCommand.USER.FromArgs,
		};
		sut = new MessageParser(cmdMap);
	}

	[Theory]
	[MemberData(nameof(TestDataCommandOnly))]
	public void CommandOnly_ShouldParse(IrcMessage expected, string input)
	{
		var result = sut.Parse(input);
		Assert.Equivalent(expected, result, true);
	}

	[Theory]
	[MemberData(nameof(TestDataCommandAndArguments))]
	public void CommandAndArguments_ShouldParse(IrcMessage expected, string input)
	{
		var result = sut.Parse(input);
		Assert.Equivalent(expected, result, true);
	}

	[Theory]
	[MemberData(nameof(TestDataCommandAndSource))]
	public void CommandAndSource_ShouldParse(IrcMessage expected, string input)
	{
		var result = sut.Parse(input);
		Assert.Equivalent(expected, result, true);
	}

	[Theory]
	[MemberData(nameof(TestDataCommandAndTags))]
	public void CommandAndTags_ShouldParse(IrcMessage expected, string input)
	{
		var result = sut.Parse(input);
		Assert.Equivalent(expected, result, true);
	}

	[Theory]
	[MemberData(nameof(TestDataMessage))]
	public void Message_ShouldParse(IrcMessage expected, string input)
	{
		var result = sut.Parse(input);
		Assert.Equivalent(expected, result, true);
	}

	[Fact]
	public void MessageEmpty_ShouldThrow()
	{
		Assert.Throws<FormatException>(() => sut.Parse(""));
	}

	[Theory]
	[InlineData("@foo=bar ")]
	[InlineData("@foo=bar;baz")]
	public void MessageTagsOnly_ShouldThrow(string input)
	{
		Assert.Throws<FormatException>(() => sut.Parse(input));
	}

	[Theory]
	[InlineData(":irc.example.com")]
	[InlineData("@foo=bar;baz :irc.example.com")]
	[InlineData("@foo :bar ")]
	public void MessageNoCommand_ShouldThrow(string input)
	{
		Assert.Throws<FormatException>(() => sut.Parse(input));
	}

	[Theory]
	[TextFileData("TestData/irc_messages.log")]
	public void VariousMessages_ShouldNotThrow(string input)
	{
		sut.Parse(input);
	}

	public static IEnumerable<object[]> TestDataCommandOnly()
	{
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.AWAY()),
			"AWAY",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.AWAY()),
			"away",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RESPONSE(IrcNumericReply.RPL_WELCOME, Array.Empty<string>())),
			"001",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.AWAY()),
			" AWAY ",
		};
	}

	public static IEnumerable<object[]> TestDataCommandAndArguments()
	{
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "" })),
			"FOO :",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "*", "LIST", "" })),
			"FOO * LIST :",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "*", "LS", "multi-prefix sasl" })),
			"FOO * LS :multi-prefix sasl",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "REQ", "sasl message-tags foo" })),
			"FOO REQ :sasl message-tags foo",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "#chan", "Hey!" })),
			"FOO #chan :Hey!",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "#chan", "Hey!" })),
			"FOO #chan Hey!",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "#chan", ":-)" })),
			"FOO #chan ::-)",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", new[] { "LS", "*", "multi-prefix extended-join sasl" })),
			"FOO LS * :multi-prefix extended-join sasl",
		};
	}

	public static IEnumerable<object[]> TestDataCommandAndSource()
	{
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Source = new IrcSource("irc.uworld.se"),
			},
			":irc.uworld.se FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Source = new IrcSource("Swishi") { User = "~Swishi", Host = "Rizon-B64320EB.t-com.sk" },
			},
			":Swishi!~Swishi@Rizon-B64320EB.t-com.sk FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Source = new IrcSource("chadghostal") { User = "~chad_ghos", Host = "B06EAEF0:BAB5E134:D38DF6E4:IP" },
			},
			":chadghostal!~chad_ghos@B06EAEF0:BAB5E134:D38DF6E4:IP FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Source = new IrcSource("CR-ARUTHA-IPv6|NEW") { User = "~sp", Host = "Arutha.New.Hot" },
			},
			":CR-ARUTHA-IPv6|NEW!~sp@Arutha.New.Hot FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Source = new IrcSource("AnimeDispenser") { User = "~desktop", Host = "Rizon-6AA4F43F.ip-37-187-118.eu" },
			},
			":AnimeDispenser!~desktop@Rizon-6AA4F43F.ip-37-187-118.eu FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Source = new IrcSource("AnimeDispenser") { Host = "Rizon-6AA4F43F.ip-37-187-118.eu" },
			},
			":AnimeDispenser@Rizon-6AA4F43F.ip-37-187-118.eu FOO",
		};
	}

	public static IEnumerable<object[]> TestDataCommandAndTags()
	{
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("aaa") },
			},
			"@aaa= FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("aaa") { Value = "bbb" } },
			},
			"@aaa=bbb FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("ccc") },
			},
			"@ccc FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("example.com/ddd") { Value = "eee" } },
			},
			"@example.com/ddd=eee FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("+example.com/foo") { Value = "bar" } },
			},
			"@+example.com/foo=bar FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("+example-client-tag") { Value = "example-value" } },
			},
			"@+example-client-tag=example-value FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("+example") { Value = @"raw+:=,escaped; \" } },
			},
			@"@+example=raw+:=,escaped\:\s\\ FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("id") { Value = "123AB" }, new("rose") },
			},
			"@id=123AB;rose FOO",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.RAW("FOO", Array.Empty<string>()))
			{
				Tags = new HashSet<IrcTag> { new("url"), new("netsplit") { Value = "tur,ty" } },
			},
			"@url=;netsplit=tur,ty FOO",
		};
	}

	public static IEnumerable<object[]> TestDataMessage()
	{
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.AWAY())
			{
				Source = new IrcSource("foo") { User = "bar", Host = "lorem.ipsum" },
			},
			":foo!bar@lorem.ipsum AWAY",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.SQUIT("foo", "shutting down"))
			{
				Source = new IrcSource("irc.example.com"),
			},
			":irc.example.com SQUIT foo :shutting down",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.PRIVMSG("#chan", "Hey what's up!"))
			{
				Tags = new HashSet<IrcTag> { new("id") { Value = "234AB" } },
				Source = new IrcSource("dan") { User = "d", Host = "localhost" },
			},
			"@id=234AB :dan!d@localhost PRIVMSG #chan :Hey what's up!",
		};
		yield return new object[]
		{
			new IrcMessage(new IrcCommand.USER("foo", 0, "barbaz")),
			"USER foo 0 * :barbaz",
		};
	}
}