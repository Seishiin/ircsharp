using System;
using System.Collections.Generic;

namespace IRCSharp.Modes;

/// <summary>
/// Represents a collection of <see cref="Modes"/>.
/// </summary>
public class IrcModes
{
	/// <summary>
	/// The <see cref="IrcMode"/>s of this object.
	/// </summary>
	public List<IrcMode> Modes { get; }

	public IrcModes(params IrcMode[] modes)
	{
		Modes = new List<IrcMode>(modes);
	}

	/// <summary>
	/// Parses a span of characters into an <see cref="IrcModes"/> object.
	/// </summary>
	/// <param name="input">A span containing the characters representing the <see cref="IrcModes"/> to convert.</param>
	/// <returns>An <see cref="IrcModes"/> object.</returns>
	public static IrcModes Parse(ReadOnlySpan<char> input)
	{
		var result = new IrcModes();

		var sign = IrcModeSign.None;
		foreach (char c in input)
		{
			if (c == '+')
				sign = IrcModeSign.Plus;

			else if (c == '-')
				sign = IrcModeSign.Minus;

			else if (sign != IrcModeSign.None && char.IsAsciiLetter(c))
				result.Modes.Add(new IrcMode(sign, c));
		}

		return result;
	}

	public override string ToString()
	{
		string result = string.Empty;
		var sign = IrcModeSign.None;

		foreach (var mode in Modes)
		{
			if (sign != mode.Sign)
			{
				sign = mode.Sign;
				result += Utils.Stringify(sign);
			}

			result += mode.Letter;
		}

		return result;
	}
}

/// <summary>
/// Modes affect the behaviour and reflect details about targets - clients and channels.
/// Modes that affect clients are called "User Modes". Modes that affect channels are
/// called "Channel Modes". Unlike user modes, there are several types of channel modes,
/// some of which may include an additional argument.
/// </summary>
/// <seealso cref="IrcModeTypes"/>
public class IrcMode
{
	/// <summary> <inheritdoc cref="IrcModeSign"/> </summary>
	public IrcModeSign Sign { get; }

	/// <summary>
	/// Represents the type of this <see cref="IrcMode"/>.
	/// </summary>
	public char Letter { get; }

	public IrcMode(IrcModeSign sign, char letter)
	{
		Sign = sign;
		Letter = letter;
	}

	public IrcMode(char letter)
	{
		Sign = IrcModeSign.Plus;
		Letter = letter;
	}

	public override string ToString() => $"{Utils.Stringify(Sign)}{Letter}";
}