using System;
using System.Linq;

namespace IRCSharp.Modes;

/// <summary>
/// Represents the modes that may be set on a channel. These modes are split into four categories, as follows:
/// <list type="number">
/// <item><see cref="A"/></item>
/// <item><see cref="B"/></item>
/// <item><see cref="C"/></item>
/// <item><see cref="D"/></item>
/// </list>
/// </summary>
public class IrcModeTypes
{
	/// <summary>
	/// Modes that add or remove an address to or from a list.
	/// These modes must always have a parameter when sent from the server to a client.
	/// A client may issue this type of mode without an argument to obtain the current contents of the list.
	/// The numerics used to retrieve contents of Type A modes depends on the specific mode.
	/// </summary>
	public char[] A { get; internal set; } = Array.Empty<char>();

	/// <summary>
	/// Modes that change a setting on a channel.
	/// These modes must always have a parameter.
	/// </summary>
	public char[] B { get; internal set; } = Array.Empty<char>();

	/// <summary>
	/// Modes that change a setting on a channel.
	/// These modes must have a parameter when being set, and must not have a parameter when being unset.
	/// </summary>
	public char[] C { get; internal set; } = Array.Empty<char>();

	/// <summary>
	/// Modes that change a setting on a channel.
	/// These modes must not have a parameter.
	/// </summary>
	/// <remarks>User modes are always this type.</remarks>
	public char[] D { get; internal set; } = Array.Empty<char>();

	internal IrcModeTypes() { }

	/// <summary>
	/// Checks if the given mode supports arguments.
	/// </summary>
	/// <param name="mode">The <see cref="IrcMode"/> to check.</param>
	/// <returns>True if the mode supports arguments; otherwise, false.</returns>
	public bool TakesArg(IrcMode mode)
	{
		if (A.Contains(mode.Letter)) return true;
		if (B.Contains(mode.Letter)) return true;
		if (C.Contains(mode.Letter)) return mode.Sign == IrcModeSign.Plus;
		return false;
	}
}