namespace IRCSharp.Modes;

/// <summary>
/// Indicates whether an <see cref="IrcMode"/> should be added or removed.
/// </summary>
public enum IrcModeSign : byte
{
	/// <summary>
	/// Not valid for <see cref="IrcMode"/>s.
	/// </summary>
	None,

	/// <summary>
	/// An <see cref="IrcMode"/> should be added.
	/// </summary>
	Plus,

	/// <summary>
	/// An <see cref="IrcMode"/> should be removed.
	/// </summary>
	Minus,
}