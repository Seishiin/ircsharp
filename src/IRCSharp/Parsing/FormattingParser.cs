using System;
using System.Linq;

namespace IRCSharp.Parsing;

internal static class FormattingParser
{
	private const char FormatColor = '\x03';
	private const char FormatColorHex = '\x04';
	private const char FormatDelim = ',';

	private static readonly char[] FormatChars =
	{
		'\x02', // Bold
		'\x1D', // Italics
		'\x1F', // Underline
		'\x1E', // Strikethrough
		'\x11', // Monospace
		'\x16', // Reverse Color
		'\x0F', // Reset
		FormatColor,
		FormatColorHex,
	};

	public static bool IsFormatted(string str)
	{
		foreach (char c in str)
		{
			if (FormatChars.Contains(c))
				return true;
		}

		return false;
	}

	public static string StripFormatting(string str)
	{
		int len = str.Length;
		Span<char> result = len > 1024 ? new char[len] : stackalloc char[len];

		int pos = 0;
		var parser = new Parser();
		foreach (char c in str)
		{
			if (parser.Next(c))
				result[pos++] = c;
		}

		return pos == len ? str : result[..pos].ToString();
	}

	private struct Parser
	{
		private State state;
		private int pos;

		public bool Next(char c)
		{
			switch (state)
			{
				case State.Text or State.Foreground when c == FormatColor:
					state = State.ColorCode;
					return false;
				case State.Text or State.ForegroundHex when c == FormatColorHex:
					state = State.ColorCodeHex;
					return false;
				case State.Text:
					return !FormatChars.Contains(c);
				case State.ColorCode when char.IsAsciiDigit(c):
					state = State.Foreground;
					pos = 0;
					return false;
				case State.Foreground when pos < 1 && char.IsAsciiDigit(c):
					pos++;
					return false;
				case State.Foreground when c == FormatDelim:
					state = State.Delimiter;
					return false;
				case State.Delimiter when char.IsAsciiDigit(c):
					state = State.Background;
					return false;
				case State.Background when char.IsAsciiDigit(c):
					state = State.Text;
					return false;
				case State.ColorCodeHex when char.IsAsciiHexDigit(c):
					state = State.ForegroundHex;
					pos = 0;
					return false;
				case State.ForegroundHex or State.BackgroundHex when pos < 5 && char.IsAsciiHexDigit(c):
					pos++;
					return false;
				case State.ForegroundHex when c == FormatDelim:
					state = State.DelimiterHex;
					return false;
				case State.DelimiterHex when char.IsAsciiHexDigit(c):
					state = State.BackgroundHex;
					pos = 0;
					return false;
				case State.BackgroundHex when char.IsAsciiHexDigit(c):
					state = State.Text;
					return false;
				default:
					state = State.Text;
					return true;
			}
		}

		private enum State
		{
			Text,
			ColorCode,
			Foreground,
			Delimiter,
			Background,
			ColorCodeHex,
			ForegroundHex,
			DelimiterHex,
			BackgroundHex,
		}
	}
}