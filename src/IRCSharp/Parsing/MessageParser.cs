using System;
using System.Collections.Generic;
using IRCSharp.Messaging;

namespace IRCSharp.Parsing;

// https://modern.ircdocs.horse/#message-format
internal sealed class MessageParser
{
	private readonly IReadOnlyDictionary<string, Func<IList<string>, IrcCommand?>> cmdMap;

	public MessageParser(IReadOnlyDictionary<string, Func<IList<string>, IrcCommand?>> cmdMap)
	{
		this.cmdMap = cmdMap;
	}

	public IrcMessage Parse(ReadOnlySpan<char> msg)
	{
		if (msg.IsEmpty) throw new FormatException("Invalid IRC message: empty message");
		var parser = new Parser(msg);

		var tags = parser.ParseTags();

		parser.SkipSpaces();
		var source = parser.ParseSource();

		parser.SkipSpaces();
		string cmdName = parser.ParseCommand();

		var args = parser.ParseArgs();

		var cmd = CreateCommand(cmdName, args);

		return new IrcMessage(cmd)
		{
			Tags = tags,
			Source = source,
		};
	}

	private IrcCommand CreateCommand(string id, IList<string> args)
	{
		if (cmdMap.TryGetValue(id, out var func))
		{
			var cmd = func.Invoke(args);
			if (cmd != null) return cmd;
		}

		var response = IrcCommand.RESPONSE.Parse(id, args);
		if (response != null) return response;

		return new IrcCommand.RAW(id, args);
	}


	private ref struct Parser
	{
		private ReadOnlySpan<char> span;

		internal Parser(ReadOnlySpan<char> span)
		{
			this.span = span;
		}

		public void SkipSpaces()
		{
			int i = 0;
			for (; i < span.Length; i++)
				if (span[i] != ' ')
					break;

			span = span[i..];
		}

		public HashSet<IrcTag> ParseTags()
		{
			var result = new HashSet<IrcTag>();
			if (span.StartsWith("@"))
			{
				int n = span.IndexOf(' ');
				if (n == -1) throw new FormatException("Invalid IRC message: no content after tags");
				var tags = span[1..n].Split(';', StringSplitOptions.RemoveEmptyEntries);

				for (int i = tags.Count - 1; i >= 0; i--)
				{
					string tag = tags[i];
					string[] parts = tag.Split('=', 2);
					result.Add(parts.Length == 1 ? new IrcTag(parts[0]) : new IrcTag(parts[0], parts[1]));
				}

				span = span[(n + 1)..];
			}

			return result;
		}

		public IrcSource? ParseSource()
		{
			IrcSource? result = null;
			if (span.StartsWith(":"))
			{
				int n = span.IndexOf(' ');
				if (n == -1) throw new FormatException("Invalid IRC message: missing command");
				result = IrcSource.Parse(span[1..n]);
				span = span[(n + 1)..];
			}

			return result;
		}

		public string ParseCommand()
		{
			if (span.IsEmpty) throw new FormatException("Invalid IRC message: missing command");

			string result;
			int n = span.IndexOf(' ');
			if (n == -1)
			{
				result = span.ToString();
				span = span[..0];
			}
			else
			{
				result = span[..n].ToString();
				span = span[n..];
			}

			return result;
		}

		public List<string> ParseArgs()
		{
			var result = new List<string>();
			if (span.IsEmpty) return result;

			string? suffix = null;
			int n = span.IndexOf(" :", StringComparison.Ordinal);
			int end = span.Length;

			if (n != -1)
			{
				suffix = span[(n + 2)..].ToString();
				end = n;
			}

			result = span[..end].Split(' ', StringSplitOptions.RemoveEmptyEntries);
			if (suffix != null)
				result.Add(suffix);

			return result;
		}
	}
}