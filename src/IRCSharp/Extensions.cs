using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using IRCSharp.Parsing;

namespace IRCSharp;

public static class Extensions
{
	/// <summary>
	/// Acts like <see cref="string.Join(char, string[])"/>, but returns <see cref="string.Empty"/> if
	/// <paramref name="collection"/> is null instead of throwing an <see cref="ArgumentNullException"/>.
	/// </summary>
	public static string Merge<T>(this IEnumerable<T>? collection, char separator)
		=> collection == null ? string.Empty : string.Join(separator, collection);

	/// <summary>
	/// Returns a value indicating whether this string contains IRC formatting characters.
	/// </summary>
	/// <returns>True if this string contains formatting characters; otherwise, false.</returns>
	public static bool IsFormatted(this string s) => FormattingParser.IsFormatted(s);

	/// <summary>
	/// Returns a new string in which all IRC formatting characters are removed.
	/// </summary>
	/// <returns>
	/// A string that is equivalent to this instance except that all formatting
	/// characters are removed. If the current instance does not contain formatting
	/// characters, the method returns the current instance unchanged.
	/// </returns>
	public static string StripFormatting(this string s) => FormattingParser.StripFormatting(s);

	internal static ConfiguredTaskAwaitable Ctx(this Task t) => t.ConfigureAwait(false);

	internal static ConfiguredValueTaskAwaitable<T> Ctx<T>(this ValueTask<T> t) => t.ConfigureAwait(false);

	internal static List<string> Split(this ReadOnlySpan<char> span, char separator, StringSplitOptions options = StringSplitOptions.None)
	{
		var res = new List<string>();
		int next = 0;
		bool last = false;

		while (!last)
		{
			int start = next;
			next = span[start..].IndexOf(separator) + start;

			last = start > next;
			if (last)
				next = span.Length;

			var slice = span[start..next];

			if ((options & StringSplitOptions.TrimEntries) != 0)
				slice = slice.Trim();

			if (!slice.IsEmpty || (options & StringSplitOptions.RemoveEmptyEntries) == 0)
				res.Add(slice.ToString());

			next++;
		}

		return res;
	}
}