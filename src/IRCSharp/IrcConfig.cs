using System;
using System.Collections.Generic;
using IRCSharp.Messaging;
using static IRCSharp.Messaging.IrcCommand;

namespace IRCSharp;

/// <summary>
/// Represents a configuration that is used to create an <see cref="IrcClient"/>.
/// </summary>
public class IrcConfig
{
	private const ushort PortPlainText = 6667;
	private const ushort PortEncrypted = 6697;

	/// <summary>
	/// The <see cref="IrcClient"/>'s nickname.
	/// </summary>
	public string NickName { get; }

	/// <summary>
	/// Alternative nicknames for the <see cref="IrcClient"/>, if the default is taken.
	/// </summary>
	public string[] AltNickNames { get; private set; } = Array.Empty<string>();

	/// <summary>
	/// The <see cref="IrcClient"/>'s username.
	/// </summary>
	public string UserName { get; private set; } = null!;

	/// <summary>
	/// The <see cref="IrcClient"/>'s real name.
	/// </summary>
	public string RealName { get; private set; } = null!;

	/// <summary>
	/// The server to connect to.
	/// </summary>
	public string ServerAddress { get; }

	/// <summary>
	/// The port to connect on. If not set, it defaults to 6697 if TLS is used, and 6667 otherwise.
	/// </summary>
	public ushort Port { get; private set; }

	/// <summary>
	/// The password to connect to the server.
	/// </summary>
	public string? Password { get; private set; }

	/// <summary>
	/// Whether or not to use TLS.
	/// </summary>
	public bool UseTls { get; private set; } = true;

	/// <summary>
	/// The text that will be sent in response to <see cref="IrcCommand.CTCP"/> VERSION requests.
	/// </summary>
	public string? Version { get; private set; }

	/// <summary>
	/// The text that will be sent in response to <see cref="IrcCommand.CTCP"/> SOURCE requests.
	/// </summary>
	public string? Source { get; private set; }

	/// <summary>
	/// The text that will be sent in response to <see cref="IrcCommand.CTCP"/> USERINFO requests.
	/// </summary>
	public string? UserInfo { get; private set; }

	internal readonly Dictionary<string, Func<IList<string>, IrcCommand?>> CmdMap = new(StringComparer.OrdinalIgnoreCase);

	internal IrcConfig(string nick, string server)
	{
		NickName = nick;
		ServerAddress = server;
		RegisterDefaultCommands();
	}

	/// <summary>
	/// Registers a custom command. Registered commands are automatically parsed
	/// and returned via <see cref="IrcClient.MessageReceived"/>.
	/// </summary>
	/// <typeparam name="T">An <see cref="IrcCommand"/>.</typeparam>
	/// <exception cref="InvalidOperationException">The command is already registered.</exception>
	public IrcConfig RegisterCommand<T>() where T : IrcCommand, IIrcCommand
	{
		if (!CmdMap.TryAdd(T.Identifier(), T.FromArgs))
		{
			throw new InvalidOperationException($"A command with the identifier '{T.Identifier()}' is already registered.");
		}

		return this;
	}

	/// <summary>
	/// Sets <see cref="AltNickNames"/> for this config.
	/// </summary>
	public IrcConfig WithAltNickNames(params string[] nickNames)
	{
		AltNickNames = nickNames;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="UserName"/> for this config.
	/// </summary>
	public IrcConfig WithUserName(string userName)
	{
		UserName = userName;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="RealName"/> for this config.
	/// </summary>
	public IrcConfig WithRealName(string realName)
	{
		RealName = realName;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="Port"/> for this config.
	/// </summary>
	public IrcConfig WithPort(ushort port)
	{
		Port = port;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="Password"/> for this config.
	/// </summary>
	public IrcConfig WithPassword(string password)
	{
		Password = password;
		return this;
	}

	/// <summary>
	/// Disables TLS for this config.
	/// </summary>
	public IrcConfig WithoutTls()
	{
		UseTls = false;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="Version"/> text for this config.
	/// </summary>
	public IrcConfig WithVersion(string version)
	{
		Version = version;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="Source"/> text for this config.
	/// </summary>
	public IrcConfig WithSource(string source)
	{
		Source = source;
		return this;
	}

	/// <summary>
	/// Sets a <see cref="UserInfo"/> text for this config.
	/// </summary>
	public IrcConfig WithUserInfo(string userInfo)
	{
		UserInfo = userInfo;
		return this;
	}

	/// <summary>
	/// Creates an <see cref="IrcClient"/> using this config.
	/// </summary>
	/// <returns>An <see cref="IrcClient"/>.</returns>
	public IrcClient Build()
	{
		if (string.IsNullOrEmpty(UserName))
			UserName = NickName;
		if (string.IsNullOrEmpty(RealName))
			RealName = UserName;
		if (Port == default)
			Port = UseTls ? PortEncrypted : PortPlainText;

		return new IrcClient(this);
	}

	private void RegisterDefaultCommands()
	{
		RegisterCommand<PASS>();
		RegisterCommand<NICK>();
		RegisterCommand<USER>();
		RegisterCommand<OPER>();
		RegisterCommand<MODE_USER>();
		RegisterCommand<SERVICE>();
		RegisterCommand<QUIT>();
		RegisterCommand<SQUIT>();
		RegisterCommand<JOIN>();
		RegisterCommand<PART>();
		// RegisterCommand<MODE_CHANNEL>(); // do not register (handled by MODE_USER)
		RegisterCommand<TOPIC>();
		RegisterCommand<NAMES>();
		RegisterCommand<LIST>();
		RegisterCommand<INVITE>();
		RegisterCommand<KICK>();
		RegisterCommand<PRIVMSG>();
		RegisterCommand<NOTICE>();
		// RegisterCommand<CTCP>(); // do not register (handled by PRIVMSG/NOTICE)
		RegisterCommand<MOTD>();
		RegisterCommand<LUSERS>();
		RegisterCommand<VERSION>();
		RegisterCommand<STATS>();
		RegisterCommand<LINKS>();
		RegisterCommand<TIME>();
		RegisterCommand<CONNECT>();
		RegisterCommand<TRACE>();
		RegisterCommand<ADMIN>();
		RegisterCommand<INFO>();
		RegisterCommand<HELP>();
		RegisterCommand<SERVLIST>();
		RegisterCommand<SQUERY>();
		RegisterCommand<WHO>();
		RegisterCommand<WHOIS>();
		RegisterCommand<WHOWAS>();
		RegisterCommand<KILL>();
		RegisterCommand<PING>();
		RegisterCommand<PONG>();
		RegisterCommand<ERROR>();
		RegisterCommand<AWAY>();
		RegisterCommand<REHASH>();
		RegisterCommand<DIE>();
		RegisterCommand<RESTART>();
		RegisterCommand<SUMMON>();
		RegisterCommand<USERS>();
		RegisterCommand<WALLOPS>();
		RegisterCommand<USERHOST>();
		RegisterCommand<ISON>();
		// RegisterCommand<RESPONSE>(); // do not register (handled directly in MessageParser.CreateCommand)
		// RegisterCommand<RAW>(); // do not register (handled directly in MessageParser.CreateCommand)
	}
}