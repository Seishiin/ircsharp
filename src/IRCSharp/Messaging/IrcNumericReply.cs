﻿// ReSharper disable InconsistentNaming

using System;

namespace IRCSharp.Messaging;

/// <summary>
/// Most messages sent from a client to a server generates a reply of some sort.
/// The most common form of reply is the numeric reply, used for both errors and normal replies.
/// </summary>
/// <remarks>
/// <para>
/// Clients must not fail because the number of parameters on a given incoming numeric is larger
/// than the number of parameters listed. Most IRC servers extends some of these numerics with
/// their own special additions.
/// </para>
/// <para>
/// Note that for numerics with "human-readable" informational strings for the last parameter
/// which are not designed to be parsed, such as in <see cref="RPL_WELCOME"/>, servers commonly
/// change this last-param text. Clients should not rely on these sort of parameters to have
/// exactly the same human-readable string as specified in the format section.
/// </para>
/// </remarks>
public enum IrcNumericReply : ushort
{
	/// <summary>
	/// The first message sent after client registration, this message introduces the client to the network.
	/// </summary>
	/// <format>(client) :Welcome to the (networkname) Network, (nick)[!(user)@(host)]</format>
	RPL_WELCOME = 001,

	/// <summary>
	/// Part of the post-registration greeting, this numeric returns the name and
	/// software/version of the server the client is currently connected to.
	/// </summary>
	///<format>(client) :Your host is (servername), running version (version)</format>
	RPL_YOURHOST = 002,

	/// <summary>
	/// Part of the post-registration greeting, this numeric returns a
	/// human-readable date/time that the server was started or created.
	/// </summary>
	///<format>(client) :This server was created (datetime)</format>
	RPL_CREATED = 003,

	/// <summary>
	/// Part of the post-registration greeting.
	/// </summary>
	/// <format>
	/// (client) (servername) (version) (available user modes)
	/// (available channel modes) [(channel modes with a parameter)]
	/// </format>
	RPL_MYINFO = 004,

	/// <summary>
	/// Designed to advertise features to clients on connection registration, providing a simple
	/// way for clients to change their behaviour based on what is implemented on the server.
	/// </summary>
	/// <format>(client) (1-13 tokens) :are supported by this server</format>
	RPL_ISUPPORT = 005,

	/// <summary>
	/// Sent to the client to redirect it to another server.
	/// </summary>
	/// <format>(client) (hostname) (port) :(info)</format>
	RPL_BOUNCE = 010,

	/// <summary>
	/// This numeric is sent by any server which handles a <see cref="IrcCommand.TRACE"/>
	/// command and has to pass it on to another server.
	/// </summary>
	/// <format>
	/// Link (version)[.(debug_level)] (destination) (next_server) [V(protocol_version)
	/// (link_uptime_in_seconds) (backstream_sendq) (upstream_sendq)] 
	/// </format>
	RPL_TRACELINK = 200,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command, this numeric is used for
	/// connections which have not been fully established and are still attempting to connect.
	/// </summary>
	/// <format>Try. (class) (server)</format>
	RPL_TRACECONNECTING = 201,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command, this numeric is used for connections
	/// which have not been fully established and are in the process of completing the server handshake.
	/// </summary>
	/// <format>H.S. (class) (server)</format>
	RPL_TRACEHANDSHAKE = 202,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command, this numeric is
	/// used for connections which have not been fully established and are unknown.
	/// </summary>
	/// <format>???? (class) [(connection_address)]</format>
	RPL_TRACEUNKNOWN = 203,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command.
	/// </summary>
	/// <format>Oper (class) (nick)</format>
	RPL_TRACEOPERATOR = 204,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command.
	/// </summary>
	/// <format>User (class) (nick)</format>
	RPL_TRACEUSER = 205,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command.
	/// </summary>
	/// <format>Serv (class) (int)S (int)C (server) (nick!user|*!*)@(host|server) [V(protocol_version)]</format>
	RPL_TRACESERVER = 206,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command.
	/// </summary>
	/// <format>Service (class) (name) (type) (active_type)</format>
	RPL_TRACESERVICE = 207,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command, this numeric is used for
	/// any connection which does not fit in the other categories but is being displayed anyway.
	/// </summary>
	/// <format>(newtype) 0 (client_name)</format>
	RPL_TRACENEWTYPE = 208,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.TRACE"/> command.
	/// </summary>
	/// <format>Class (class) (count)</format>
	RPL_TRACECLASS = 209,

	/// <summary>
	/// Unused.
	/// </summary>
	RPL_TRACERECONNECT = 210,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.STATS"/> command.
	/// </summary>
	/// <format>(linkname) (sendq) (sent_msgs) (sent_bytes) (recvd_msgs) (rcvd_bytes) (time_open)</format>
	RPL_STATSLINKINFO = 211,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.STATS"/> command.
	/// </summary>
	/// <format>(command) (count) [(byte_count) (remote_count)]</format>
	RPL_STATSCOMMANDS = 212,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.STATS"/> command,
	/// this numeric indicates the end of a STATS response.
	/// </summary>
	/// <format>(stats) :End of /STATS report</format>
	RPL_ENDOFSTATS = 219,

	/// <summary>
	/// Sent to a client to inform that client of their currently-set user modes.
	/// </summary>
	/// <format>(client) (user modes)</format>
	RPL_UMODEIS = 221,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.SERVLIST"/> command.
	/// </summary>
	/// <format>(name) (server) (mask) (type) (hopcount) (info)</format>
	RPL_SERVLIST = 234,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.SERVLIST"/> command, this
	/// numeric indicates the end of a <see cref="RPL_SERVLIST"/> response.
	/// </summary>
	/// <format>(mask) (type) :End of service listing</format>
	RPL_SERVLISTEND = 235,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.STATS"/> command,
	/// this numeric reports the server uptime.
	/// </summary>
	/// <format>:Server Up (days) days (hours):(minutes):(seconds)</format>
	RPL_STATSUPTIME = 242,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.STATS"/> command, this numeric
	/// reports the allowed hosts from where users may become IRC operators.
	/// </summary>
	/// <format>O (hostmask) * (nick) [:(info)]</format>
	RPL_STATSOLINE = 243,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (u), (i), and (s) are
	/// non-negative integers, and represent the number of total users, invisible users,
	/// and other servers connected to this server.
	/// </summary>
	/// <format>(client) :There are (u) users and (i) invisible on (s) servers</format>
	RPL_LUSERCLIENT = 251,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (ops) is a positive
	/// integer and represents the number of IRC operators connected to this server.
	/// </summary>
	/// <format>(client) (ops) :operators online</format>
	RPL_LUSEROP = 252,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (connections) is a positive
	/// integer and represents the number of connections to this server that are currently in an unknown state. 
	/// </summary>
	/// <format>(client) (connections) :unknown connections</format>
	RPL_LUSERUNKNOWN = 253,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (channels) is a positive
	/// integer and represents the number of channels that currently exist on this server.
	/// </summary>
	/// <format>(client) (channels) :channels formed</format>
	RPL_LUSERCHANNELS = 254,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (c) and (s) are non-negative integers
	/// and represent the number of clients and other servers connected to this server, respectively.
	/// </summary>
	/// <format>(client) :I have (c) clients and (s) servers</format>
	RPL_LUSERME = 255,

	/// <summary>
	/// Sent as a reply to an <see cref="IrcCommand.ADMIN"/> command, this numeric establishes
	/// the name of the server whose administrative info is being provided. 
	/// </summary>
	/// <format>(client) [(server)] :Administrative info</format>
	RPL_ADMINME = 256,

	/// <summary>
	/// Sent as a reply to an <see cref="IrcCommand.ADMIN"/> command, (info) is a string
	/// intended to provide information about the location of the server.
	/// </summary>
	/// <format>(client) :(info)</format>
	RPL_ADMINLOC1 = 257,

	/// <summary>
	/// Sent as a reply to an <see cref="IrcCommand.ADMIN"/> command, (info) is a string
	/// intended to provide information about whoever runs the server.
	/// </summary>
	/// <format>(client) :(info)</format>
	RPL_ADMINLOC2 = 258,

	/// <summary>
	/// Sent as a reply to an <see cref="IrcCommand.ADMIN"/> command, (info) contains
	/// the email address to contact the administrators of the server.
	/// </summary>
	/// <format>(client) :(info)</format>
	RPL_ADMINEMAIL = 259,

	/// <summary>
	/// Sent as a reply to an <see cref="IrcCommand.TRACE"/> command.
	/// </summary>
	/// <format>File (logfile) (debug_level)</format>
	RPL_TRACELOG = 261,

	/// <summary>
	/// Sent as a reply to an <see cref="IrcCommand.TRACE"/> command,
	/// this numeric indicates the end of a TRACE response.
	/// </summary>
	/// <format>(server_name) (version)[.(debug_level)] :End of /TRACE list</format>
	RPL_TRACEEND = 262,

	/// <summary>
	/// When a server drops a command without processing it, this numeric will be sent to inform
	/// the client. The text used in the last param of this message commonly provides the client
	/// with more information about why the command could not be processed
	/// </summary>
	/// <format>(client) (command) :Please wait a while and try again.</format>
	RPL_TRYAGAIN = 263,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (u) and (m) are non-negative
	/// integers and represent the number of clients currently and the maximum number of clients
	/// that have been connected directly to this server at one time, respectively.
	/// </summary>
	/// <format>(client) [(u) (m)] :Current local users (u), max (m)</format>
	RPL_LOCALUSERS = 265,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LUSERS"/> command. (u) and (m) are non-negative integers.
	/// (u) represents the number of clients currently connected to this server, globally.
	/// (m) represents the maximum number of clients that have been connected to this server at one time, globally.
	/// </summary>
	/// <format>(client) [(u) (m)] :Current global users (u), max (m)</format>
	RPL_GLOBALUSERS = 266,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric shows the SSL/TLS
	/// certificate fingerprint used by the client with the nickname (nick).
	/// </summary>
	/// <format>(client) (nick) :has client certificate fingerprint (fingerprint)</format>
	RPL_WHOISCERTFP = 276,

	/// <summary>
	/// This is a dummy numeric. It does not have a defined use nor format.
	/// </summary>
	/// <format>Undefined format</format>
	RPL_NONE = 300,

	/// <summary>
	/// Indicates that the user with the nickname (nick) is currently
	/// away and sends the away message that they set.
	/// </summary>
	/// <format>(client) (nick) :(message)</format>
	RPL_AWAY = 301,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.USERHOST"/> command, this numeric lists nicknames
	/// and the information associated with them. The last parameter of this numeric is a list of
	/// (reply) values, delimited by a SPACE character.
	/// </summary>
	/// <format>(client) :[(reply){ (reply)}]</format>
	RPL_USERHOST = 302,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.ISON"/> command.
	/// </summary>
	/// <format>:[(nick) { (nick)}]</format>
	RPL_ISON = 303,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.AWAY"/> command, this
	/// lets the client know that they are no longer set as being away.
	/// </summary>
	/// <format>(client) :You are no longer marked as being away</format>
	RPL_UNAWAY = 305,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.AWAY"/> command, this
	/// lets the client know that they are set as being away.
	/// </summary>
	/// <format>(client) :You have been marked as being away</format>
	RPL_NOWAWAY = 306,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric
	/// indicates that the client with the nickname (nick) was authenticated as the
	/// owner of this nick on the network.
	/// </summary>
	/// <format>(client) (nick) :has identified for this nick</format>
	RPL_WHOISREGNICK = 307,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric shows details
	/// about the client with the nickname (nick). (username) and (realname) represent the names
	/// set by the <see cref="IrcCommand.USER"/> command. (host) represents the host used for the
	/// client in nickmasks. (host) cannot start with a colon ':' as this would get parsed as a
	/// trailing parameter - IPv6 addresses such as "::1" are prefixed with a zero '0' to ensure this.
	/// The second-last parameter is the character '*' and does not mean anything.
	/// </summary>
	/// <format>(client) (nick) (username) (host) * :(realname)</format>
	RPL_WHOISUSER = 311,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> / <see cref="IrcCommand.WHOWAS"/> command,
	/// this numeric shows which server the client with the nickname (nick) is/was connected to.
	/// (server) is the name of the server. (server info) is a string containing a description of that server.
	/// </summary>
	/// <format>(client) (nick) (server) :(server info)</format>
	RPL_WHOISSERVER = 312,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric indicates that the client
	/// with the nickname (nick) is an operator. This command may also indicate what type or level of
	/// operator the client is by changing the text in the last parameter of this numeric.
	/// </summary>
	/// <format>(client) (nick) :is an IRC operator</format>
	RPL_WHOISOPERATOR = 313,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOWAS"/> command, this numeric shows details about one of
	/// the last clients that used the nickname (nick). The purpose of each argument is the same
	/// as with the <see cref="RPL_WHOISUSER"/> numeric.
	/// </summary>
	/// <format>(client) (nick) (username) (host) * :(realname)</format>
	RPL_WHOWASUSER = 314,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHO"/> command, this numeric indicates the
	/// end of a <see cref="RPL_WHOREPLY"/> for the mask (mask). (mask) must be the same (mask)
	/// parameter sent by the client in its <see cref="IrcCommand.WHO"/> command, but may be casefolded.
	/// </summary>
	/// <format>(client) (mask) :End of /WHO list</format>
	RPL_ENDOFWHO = 315,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric indicates how long the
	/// client with the nickname (nick) has been idle. (secs) is the number of seconds since the client
	/// has been active. (signon) is a unix timestamp representing when the user joined the network.
	/// </summary>
	/// <format>(client) (nick) (secs) (signon) :seconds idle, signon time</format>
	RPL_WHOISIDLE = 317,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric indicates the end
	/// of a WHOIS response for the client with the nickname (nick).
	/// </summary>
	/// <format>(client) (nick) :End of /WHOIS list</format>
	RPL_ENDOFWHOIS = 318,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric lists the channels that the
	/// client with the nickname (nick) is joined to and their status in these channels. (prefix) is the highest
	/// channel membership prefix that the client has in that channel, if the client has one. (channel) is the
	/// name of a channel that the client is joined to. The last parameter of this numeric is a list of
	/// [prefix](channel) pairs, delimited by a SPACE character.
	/// </summary>
	/// <format>(client) (nick) :[prefix](channel){ [prefix](channel)}</format>
	RPL_WHOISCHANNELS = 319,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric is used
	/// for extra human-readable information on the client with nickname (nick).
	/// </summary>
	/// <format>(client) (nick) :blah blah blah</format>
	RPL_WHOISSPECIAL = 320,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LIST"/> command, this numeric marks the start of a channel
	/// list. This numeric may be skipped by the server so clients must not depend on receiving it.
	/// </summary>
	/// <format>(client) Channel :Users  Name</format>
	RPL_LISTSTART = 321,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LIST"/> command, this numeric sends information
	/// about a channel to the client. (channel) is the name of the channel. (client count) is an
	/// integer indicating how many clients are joined to that channel. (topic) is the channel topic.
	/// </summary>
	/// <format>(client) (channel) (client count) :(topic)</format>
	RPL_LIST = 322,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LIST"/> command, this numeric
	/// indicates the end of a <see cref="RPL_LIST"/>.
	/// </summary>
	/// <format>(client) :End of /LIST</format>
	RPL_LISTEND = 323,

	/// <summary>
	/// Sent to a client to inform them of the currently-set modes of a channel. (channel) is
	/// the name of the channel. (modestring) and (mode arguments) are a mode string and
	/// the mode arguments delimited as separate parameters.
	/// </summary>
	/// <format>(client) (channel) (modestring) (mode arguments)</format>
	RPL_CHANNELMODEIS = 324,

	/// <summary>
	/// Indicates that a command failed because the client does not have "channel creator" channel privileges.
	/// </summary>
	/// <format>(channel) (nickname)</format>
	RPL_UNIQOPIS = 325,

	/// <summary>
	/// Sent to a client to inform them of the creation time of a channel. (channel) is
	/// the name of the channel. (creationtime) is a unix timestamp representing when
	/// the channel was created on the network.
	/// </summary>
	/// <format>(client) (channel) (creationtime)</format>
	RPL_CREATIONTIME = 329,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric indicates that the client
	/// with the nickname (nick) was authenticated as the owner of (account). This does not necessarily mean
	/// the user owns their current nickname, which is covered by <see cref="RPL_WHOISREGNICK"/>.
	/// </summary>
	/// <format>(client) (nick) (account) :is logged in as</format>
	RPL_WHOISACCOUNT = 330,

	/// <summary>
	/// Sent to a client when joining a channel to inform them that the channel
	/// with the name (channel) does not have any topic set.
	/// </summary>
	/// <format>(client) (channel) :No topic is set</format>
	RPL_NOTOPIC = 331,

	/// <summary>
	/// Sent to a client when joining the (channel) to inform them of the current topic of the channel.
	/// </summary>
	/// <format>(client) (channel) :(topic)</format>
	RPL_TOPIC = 332,

	/// <summary>
	/// Sent to a client to let them know who set the topic and when
	/// they set it. Sent after <see cref="RPL_TOPIC"/>.
	/// </summary>
	/// <format>(client) (channel) (nick) (setat)</format>
	RPL_TOPICWHOTIME = 333,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command,
	/// this numeric indicates that the given user is a bot.
	/// </summary>
	/// <format>(client) (target) :is a bot</format>
	RPL_WHOISBOT = 335,

	/// <summary>
	/// Sent to a client as a reply to the <see cref="IrcCommand.INVITE"/> command when used
	/// with no parameter, to indicate a channel the client was invited to.
	/// </summary>
	/// <remarks>This numeric should not be confused with <see cref="RPL_INVEXLIST"/>.</remarks>
	/// <format>(client) (channel)</format>
	RPL_INVITELIST = 336,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.INVITE"/> command when used with no
	/// parameter, this numeric indicates the end of invitations a client received.
	/// </summary>
	/// <remarks>This numeric should not be confused with <see cref="RPL_ENDOFINVEXLIST"/>.</remarks>
	/// <format>(client) :End of /INVITE list</format>
	RPL_ENDOFINVITELIST = 337,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> and <see cref="IrcCommand.WHOWAS"/>
	/// commands, this numeric shows details about the client with the nickname (nick).
	/// </summary>
	/// <format>
	/// <para>(client) (nick) :is actually...</para>
	/// <para>(client) (nick) (host|ip) :is actually using host</para>
	/// <para>(client) (nick) (username)@(hostname) (ip) :is actually using host</para>
	/// </format>
	RPL_WHOISACTUALLY = 338,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.INVITE"/> command to indicate that the attempt
	/// was successful and the client with the nickname (nick) has been invited to (channel).
	/// </summary>
	/// <format>(client) (nick) (channel)</format>
	RPL_INVITING = 341,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.SUMMON"/> command
	/// to indicate that the server is summoning that user.
	/// </summary>
	/// <format>(user) :Summoning user to IRC</format>
	RPL_SUMMONING = 342,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.MODE_CHANNEL"/> command, when clients are viewing the current
	/// entries on a channel's invite-exception list. (mask) is the given mask on the invite-exception list.
	/// </summary>
	/// <remarks>This numeric should not be confused with <see cref="RPL_INVITELIST"/>.</remarks>
	/// <format>(client) (channel) (mask)</format>
	RPL_INVEXLIST = 346,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.MODE_CHANNEL"/> command, this
	/// numeric indicates the end of a channel's invite-exception list.
	/// </summary>
	/// <remarks>This numeric should not be confused with <see cref="RPL_ENDOFINVITELIST"/>.</remarks>
	/// <format>(client) (channel) :End of Channel Invite Exception List</format>
	RPL_ENDOFINVEXLIST = 347,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.MODE_CHANNEL"/> command, when
	/// clients are viewing the current entries on a channel's exception list.
	/// (mask) is the given mask on the exception list.
	/// </summary>
	/// <format>(client) (channel) (mask)</format>
	RPL_EXCEPTLIST = 348,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.MODE_CHANNEL"/> command,
	/// this numeric indicates the end of a channel's exception list.
	/// </summary>
	/// <format>(client) (channel) :End of channel exception list</format>
	RPL_ENDOFEXCEPTLIST = 349,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.VERSION"/> command, this numeric indicates information
	/// about the desired server. (version) is the name and version of the software being used.
	/// (server) is the name of the server. (comments) may contain any further comments or details
	/// about the specific version of the server.
	/// </summary>
	/// <format>(client) (version) (server) :(comments)</format>
	RPL_VERSION = 351,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHO"/> command, this numeric gives information
	/// about the client with the nickname (nick). Refer to <see cref="RPL_WHOISUSER"/> for the
	/// meaning of the fields (username), (host) and (realname). (server) is the name of the
	/// server the client is connected to. (channel) is an arbitrary channel the client is joined
	/// to or the character '*' if no channel is returned.
	/// </summary>
	/// <format>(client) (channel) (username) (host) (server) (nick) (flags) :(hopcount) (realname)</format>
	RPL_WHOREPLY = 352,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.NAMES"/> command, this numeric lists the
	/// clients that are joined to (channel) and their status in that channel.
	/// </summary>
	/// <format>(client) (symbol) (channel) :[prefix](nick){ [prefix](nick)}</format>
	RPL_NAMREPLY = 353,

	/// <summary>
	/// Exactly the fields requested by the client must be returned by the server. When the server
	/// omits a field the client has requested, the following placeholders must be used:
	/// <list type="bullet">
	/// <item>'*' is used for [channel] when no channel is returned.</item>
	/// <item>"255.255.255.255" is used for [ip] when the server refuses to disclose the IP address.</item>
	/// <item>'0' is used for [account] when the user is logged out.</item>
	/// <item>Any syntactically correct placeholder can be used for [oplevel] when the server does not support op levels.</item>
	/// </list>
	/// </summary>
	/// <remarks>
	/// Clients should ignore the values of the hop count (d) and the channel op level (o) fields,
	/// because they are ill-defined and unreliable.
	/// </remarks>
	/// <format>
	/// (client) [token] [channel] [user] [ip] [host] [server] [nick]
	/// [flags] [hopcount] [idle] [account] [oplevel] [:realname]
	/// </format>
	RPL_WHOSPCRPL = 354,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LINKS"/> command, this numeric
	/// specifies one of the known servers on the network. (server info) is a
	/// string containing a description of that server.
	/// </summary>
	/// <format>(client) * (server) :(hopcount) (server info)</format>
	RPL_LINKS = 364,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.LINKS"/> command, this numeric
	/// specifies the end of a list of channel member names.
	/// </summary>
	/// <format>(client) * :End of /LINKS list</format>
	RPL_ENDOFLINKS = 365,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.NAMES"/> command, this
	/// numeric specifies the end of a list of channel member names.
	/// </summary>
	/// <format>(client) (channel) :End of /NAMES list</format>
	RPL_ENDOFNAMES = 366,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.MODE_CHANNEL"/> command, when clients are viewing the current
	/// entries on a channel's ban list. (mask) is the given mask on the ban list. (who) and (set-ts) are optional
	/// and may be included in responses. (who) is either the nickname or nickmask of the client that set the ban,
	/// or a server name, and (set-ts) is the UNIX timestamp of when the ban was set.
	/// </summary>
	/// <format>(client) (channel) (mask) [(who) (set-ts)]</format>
	RPL_BANLIST = 367,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.MODE_CHANNEL"/> command,
	/// this numeric indicates the end of a channel's ban list.
	/// </summary>
	/// <format>(client) (channel) :End of channel ban list</format>
	RPL_ENDOFBANLIST = 368,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOWAS"/> command, this numeric indicates
	/// the end of a <see cref="IrcCommand.WHOWAS"/> reponse for the nickname (nick). This
	/// numeric is sent after all other WHOWAS response numerics have been sent to the client.
	/// </summary>
	/// <format>(client) (nick) :End of /WHOWAS</format>
	RPL_ENDOFWHOWAS = 369,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.INFO"/> command, this numeric returns human-readable
	/// information describing the server: e.g. its version, list of authors and contributors,
	/// and any other miscellaneous information which may be considered to be relevant.
	/// </summary>
	/// <format>(client) :(string)</format>
	RPL_INFO = 371,

	/// <summary>
	/// When sending the Message of the Day to the client, servers reply with each line of the
	/// MOTD as this numeric. MOTD lines may be wrapped to 80 characters by the server.
	/// </summary>
	/// <format>(client) :(line of the motd)</format>
	RPL_MOTD = 372,

	/// <summary>
	/// Indicates the end of an INFO response.
	/// </summary>
	/// <format>(client) :End of /INFO list</format>
	RPL_ENDOFINFO = 374,

	/// <summary>
	/// Indicates the start of the Message of the Day to the client.
	/// </summary>
	/// <format>(client) :- (server) Message of the day -</format>
	RPL_MOTDSTART = 375,

	/// <summary>
	/// Indicates the end of the Message of the Day to the client.
	/// </summary>
	/// <format>(client) :End of /MOTD command.</format>
	RPL_ENDOFMOTD = 376,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric
	/// shows details about where the client with nickname (nick) is connecting from.
	/// </summary>
	/// <format>(client) (nick) :is connecting from *@localhost 127.0.0.1</format>
	RPL_WHOISHOST = 378,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this
	/// numeric shows the client what user modes the target users has.
	/// </summary>
	/// <format>(client) (nick) :is using modes (modes)</format>
	RPL_WHOISMODES = 379,

	/// <summary>
	/// Sent to a client which has just successfully issued an <see cref="IrcCommand.OPER"/>
	/// command and gained operator status.
	/// </summary>
	/// <format>(client) :You are now an IRC operator</format>
	RPL_YOUREOPER = 381,

	/// <summary>
	/// Sent to an operator which has just successfully issued a <see cref="IrcCommand.REHASH"/> command.
	/// </summary>
	/// <format>(client) (config file) :Rehashing</format>
	RPL_REHASHING = 382,

	/// <summary>
	/// Sent upon successful registration of a service.
	/// </summary>
	/// <format>:You are service (service_name)</format>
	RPL_YOURESERVICE = 383,

	/// <summary>
	/// Reply to the <see cref="IrcCommand.TIME"/> command. Typically only contains the human-readable
	/// time, but it may include a UNIX timestamp. (TS offset) is used by some servers using a TS-based
	/// server-to-server protocol, and represents the offset between the server's system time, and the
	/// TS of the network. A positive value means the server is lagging behind the TS of the network.
	/// </summary>
	/// <format>(client) (server) [(timestamp) [(TS offset)]] :(human-readable time)</format>
	RPL_TIME = 391,

	/// <summary>
	/// Indicates the start of a reply to a <see cref="IrcCommand.USERS"/> command.
	/// </summary>
	/// <format>:UserID Terminal Host</format>
	RPL_USERSSTART = 392,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.USERS"/> command.
	/// </summary>
	/// <format>:(username) (ttyline) (hostname)</format>
	RPL_USERS = 393,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.USERS"/> command, this numeric
	/// indicates the end of a <see cref="RPL_USERS"/>.
	/// </summary>
	/// <format>:End of users</format>
	RPL_ENDOFUSERS = 394,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.USERS"/> command,
	/// this numeric indicates that nobody is logged in. 
	/// </summary>
	/// <format>:Nobody logged in</format>
	RPL_NOUSERS = 395,

	/// <summary>
	/// Indicates that the given command/subcommand could not be processed.
	/// (subcommand) may repeat for more specific subcommands.
	/// </summary>
	/// <format>(client) (command){ (subcommand)} :(info)</format>
	ERR_UNKNOWNERROR = 400,

	/// <summary>
	/// Indicates that no client can be found for the supplied nickname.
	/// </summary>
	/// <format>(client) (nickname) :No such nick/channel</format>
	ERR_NOSUCHNICK = 401,

	/// <summary>
	/// Indicates that the given server name does not exist.
	/// </summary>
	/// <format>(client) (server name) :No such server</format>
	ERR_NOSUCHSERVER = 402,

	/// <summary>
	/// Indicates that no channel can be found for the supplied channel name.
	/// </summary>
	/// <format>(client) (channel) :No such channel</format>
	ERR_NOSUCHCHANNEL = 403,

	/// <summary>
	/// Indicates that the <see cref="IrcCommand.PRIVMSG"/> / <see cref="IrcCommand.NOTICE"/>
	/// could not be delivered to (channel). This is generally sent in response to channel modes,
	/// such as a channel being moderated and the client not having permission to speak on the
	/// channel, or not being joined to a channel with the no external messages mode set.
	/// </summary>
	/// <format>(client) (channel) :Cannot send to channel</format>
	ERR_CANNOTSENDTOCHAN = 404,

	/// <summary>
	/// Indicates that the <see cref="IrcCommand.JOIN"/> command failed because
	/// the client has joined their maximum number of channels.
	/// </summary>
	/// <format>(client) (channel) :You have joined too many channels</format>
	ERR_TOOMANYCHANNELS = 405,

	/// <summary>
	/// Sent as a reply to a <see cref="IrcCommand.WHOWAS"/> command to
	/// indicate there is no history information for that nickname.
	/// </summary>
	/// <format>(client) :There was no such nickname</format>
	ERR_WASNOSUCHNICK = 406,

	/// <summary>
	/// Indicates that the given targets for a command are ambiguous in that they relate to too many targets.
	/// </summary>
	/// <format>(target) :(error_code) recipients. (abort_message)</format>
	ERR_TOOMANYTARGETS = 407,

	/// <summary>
	/// Sent to a client which is attempting to send an <see cref="IrcCommand.SQUERY"/>
	/// (or other command) to a service which does not exist.
	/// </summary>
	/// <format>(service_name) :No such service</format>
	ERR_NOSUCHSERVICE = 408,

	/// <summary>
	/// Indicates a <see cref="IrcCommand.PING"/> / <see cref="IrcCommand.PONG"/> command
	/// missing the originator parameter which is required by old IRC servers.
	/// Nowadays, this may be used by some servers when the PING (token) is empty.
	/// </summary>
	/// <format>(client) :No origin specified</format>
	ERR_NOORIGIN = 409,

	/// <summary>
	/// Returned when no recipient is given with a command.
	/// </summary>
	/// <format>:No recipient given [(command)]</format>
	ERR_NORECIPIENT = 411,

	/// <summary>
	/// Returned when <see cref="IrcCommand.PRIVMSG"/> / <see cref="IrcCommand.NOTICE"/>
	/// is used with no message given.
	/// </summary>
	/// <format>:No text to send</format>
	ERR_NOTEXTTOSEND = 412,

	/// <summary>
	/// Used when a message is being sent to a mask without being limited to a top-level domain.
	/// </summary>
	/// <format>(mask) :No toplevel domain specified</format>
	ERR_NOTOPLEVEL = 413,

	/// <summary>
	/// Used when a message is being sent to a mask with a wildcard for a top level domain.
	/// </summary>
	/// <format>(mask) :Wildcard in toplevel domain</format>
	ERR_WILDTOPLEVEL = 414,

	/// <summary>
	/// Used when a message is being sent to a mask with an invalid syntax.
	/// </summary>
	/// <format>(mask) :Bad server/host mask</format>
	ERR_BADMASK = 415,

	/// <summary>
	/// Indicates a given line does not follow the specified size limits
	/// (512 bytes for the main section, 4094 or 8191 bytes for the tag section).
	/// </summary>
	/// <format>(client) :Input line was too long</format>
	ERR_INPUTTOOLONG = 417,

	/// <summary>
	/// Sent to a registered client to indicate that the command they sent isn't known by the server.
	/// </summary>
	/// <format>(client) (command) :Unknown command</format>
	ERR_UNKNOWNCOMMAND = 421,

	/// <summary>
	/// Indicates that the Message of the Day file does not exist or could not be found.
	/// </summary>
	/// <format>(client) :MOTD File is missing</format>
	ERR_NOMOTD = 422,

	/// <summary>
	/// Sent as a reply to a <see cref="IrcCommand.ADMIN"/>
	/// command to indicate that no information is available.
	/// </summary>
	/// <format>(server) :No administrative info available</format>
	ERR_NOADMININFO = 423,

	/// <summary>
	/// Generic error message used to report a failed file operation during the processing of a command.
	/// </summary>
	/// <format>:File error doing (file_op) on (file)</format>
	ERR_FILEERROR = 424,

	/// <summary>
	/// Returned when a nickname parameter expected for a command is not found.
	/// </summary>
	/// <format>:No nickname given</format>
	ERR_NONICKNAMEGIVEN = 431,

	/// <summary>
	/// Returned when a <see cref="IrcCommand.NICK"/> command cannot be successfully completed
	/// as the desired nickname contains characters that are disallowed by the server.
	/// </summary>
	/// <format>(client) (nick) :Erroneus nickname</format>
	ERR_ERRONEUSNICKNAME = 432,

	/// <summary>
	/// Returned when a <see cref="IrcCommand.NICK"/> command cannot be successfully
	/// completed as the desired nickname is already in use on the network.
	/// </summary>
	/// <format>(client) (nick) :Nickname is already in use</format>
	ERR_NICKNAMEINUSE = 433,

	/// <summary>
	/// Returned by a server to a client when it detects a nickname collision.
	/// </summary>
	/// <format>(nick) :Nickname collision KILL from (user)@(host)</format>
	ERR_NICKCOLLISION = 436,

	/// <summary>
	/// Returned by a server to a user trying to join a channel or change nickname
	/// which is blocked by the channel/nick delay mechanism.
	/// </summary>
	/// <format>(nick/channel) :Nick/channel is temporarily unavailable</format>
	ERR_UNAVAILRESOURCE = 437,

	/// <summary>
	/// Returned when a client tries to perform a channel+nick affecting command,
	/// when the nick isn't joined to the channel.
	/// </summary>
	/// <format>(client) (nick) (channel) :They aren't on that channel</format>
	ERR_USERNOTINCHANNEL = 441,

	/// <summary>
	/// Returned when a client tries to perform a channel-affecting
	/// command on a channel which the client isn't a part of.
	/// </summary>
	/// <format>(client) (channel) :You're not on that channel</format>
	ERR_NOTONCHANNEL = 442,

	/// <summary>
	/// Returned when a client tries to invite (nick) to a channel they're already joined to.
	/// </summary>
	/// <format>(client) (nick) (channel) :is already on channel</format>
	ERR_USERONCHANNEL = 443,

	/// <summary>
	/// Sent as a reply to a <see cref="IrcCommand.SUMMON"/> command to
	/// indicate that (user) was not logged in and could not be summoned.
	/// </summary>
	/// <format>(user) :User not logged in</format>
	ERR_NOLOGIN = 444,

	/// <summary>
	/// Sent as a reply to a <see cref="IrcCommand.SUMMON"/> command to
	/// indicate that it has been disabled or not implemented.
	/// </summary>
	/// <format>:SUMMON has been disabled</format>
	ERR_SUMMONDISABLED = 445,

	/// <summary>
	/// Sent as a reply to a <see cref="IrcCommand.USERS"/> command to
	/// indicate that it has been disabled or not implemented.
	/// </summary>
	/// <format>:USERS has been disabled</format>
	ERR_USERSDISABLED = 446,

	/// <summary>
	/// Returned when a client command cannot be parsed as they are not yet
	/// registered. Servers offer only a limited subset of commands until
	/// clients are properly registered to the server.
	/// </summary>
	/// <format>(client) :You have not registered</format>
	ERR_NOTREGISTERED = 451,

	/// <summary>
	/// Returned when a client command cannot be parsed because not enough parameters were supplied.
	/// </summary>
	/// <format>(client) (command) :Not enough parameters</format>
	ERR_NEEDMOREPARAMS = 461,

	/// <summary>
	/// Returned when a client tries to change a detail that can only be set during registration.
	/// </summary>
	/// <format>(client) :You may not reregister</format>
	ERR_ALREADYREGISTRED = 462,

	/// <summary>
	/// Returned to a client which attempts to register with a server which
	/// has been configured to refuse connections from the client's host.
	/// </summary>
	/// <format>:Your host isn't among the privileged</format>
	ERR_NOPERMFORHOST = 463,

	/// <summary>
	/// Returned to indicate that the connection could not be registered
	/// as the password was either incorrect or not supplied.
	/// </summary>
	/// <format>(client) :Password incorrect</format>
	ERR_PASSWDMISMATCH = 464,

	/// <summary>
	/// Returned to indicate that the server has been configured to explicitly deny connections from this client.
	/// </summary>
	/// <format>(client) :You are banned from this server.</format>
	ERR_YOUREBANNEDCREEP = 465,

	/// <summary>
	/// Sent by a server to a user to inform that access to the server will soon be denied.
	/// </summary>
	ERR_YOUWILLBEBANNED = 466,

	/// <summary>
	/// Returned when the channel key for a (channel) has already been set.
	/// </summary>
	/// <format>(channel) :Channel key already set</format>
	ERR_KEYSET = 467,

	/// <summary>
	/// Returned to indicate that a <see cref="IrcCommand.JOIN"/> command failed because the client
	/// limit mode has been set and the maximum number of users are already joined to the channel.
	/// </summary>
	/// <format>(client) (channel) :Cannot join channel (+l)</format>
	ERR_CHANNELISFULL = 471,

	/// <summary>
	/// Indicates that a mode character used by a client is not recognized by the server.
	/// </summary>
	/// <format>(client) (modechar) :is unknown mode char to me</format>
	ERR_UNKNOWNMODE = 472,

	/// <summary>
	/// Returned to indicate that a <see cref="IrcCommand.JOIN"/> command failed because
	/// the channel is set to [invite-only] mode and the client has not been invited
	/// to the channel or had an invite exception set for them.
	/// </summary>
	/// <format>(client) (channel) :Cannot join channel (+i)</format>
	ERR_INVITEONLYCHAN = 473,

	/// <summary>
	/// Returned to indicate that a <see cref="IrcCommand.JOIN"/> command failed because the
	/// client has been banned from the channel and has not had a ban exception set for them.
	/// </summary>
	/// <format>(client) (channel) :Cannot join channel (+b)</format>
	ERR_BANNEDFROMCHAN = 474,

	/// <summary>
	/// Returned to indicate that a <see cref="IrcCommand.JOIN"/> command failed because
	/// the channel requires a key and the key was either incorrect or not supplied.
	/// </summary>
	/// <remarks>Not to be confused with <see cref="ERR_INVALIDKEY"/>, which may be returned when setting a key.</remarks>
	/// <format>(client) (channel) :Cannot join channel (+k)</format>
	ERR_BADCHANNELKEY = 475,

	/// <summary>
	/// Indicates the supplied channel name is not valid. This is similar to,
	/// but stronger than, <see cref="ERR_NOSUCHCHANNEL"/>, which indicates that
	/// the channel does not exist, but that it may be a valid name.
	/// </summary>
	/// <format>(channel) :Bad Channel Mask</format>
	ERR_BADCHANMASK = 476,

	/// <summary>
	/// Returned when attempting to set a mode on a channel which does not support channel modes, or channel mode changes.
	/// </summary>
	/// <format>(channel) :Channel doesn't support modes</format>
	ERR_NOCHANMODES = 477,

	/// <summary>
	/// Returned when a channel access list is full and cannot be added to.
	/// </summary>
	/// <format>(channel) (char) :Channel list is full</format>
	ERR_BANLISTFULL = 478,

	/// <summary>
	/// Indicates that the command failed because the user is not an IRC operator.
	/// </summary>
	/// <format>(client) :Permission Denied - You're not an IRC operator</format>
	ERR_NOPRIVILEGES = 481,

	/// <summary>
	/// Indicates that a command failed because the client does not have the appropriate channel privileges.
	/// This numeric can apply for different prefixes such as halfop, operator, etc.
	/// </summary>
	/// <format>(client) (channel) :You're not channel operator</format>
	ERR_CHANOPRIVSNEEDED = 482,

	/// <summary>
	/// Indicates that a <see cref="IrcCommand.KILL"/> command failed because the user tried to kill a server.
	/// </summary>
	/// <format>(client) :You can't kill a server!</format>
	ERR_CANTKILLSERVER = 483,

	/// <summary>
	/// Sent by the server to a user upon connection to indicate the restricted nature of the connection.
	/// </summary>
	/// <format>:Your connection is restricted!</format>
	ERR_RESTRICTED = 484,

	/// <summary>
	/// Any MODE requiring chanop privileges must return this error if the client making the attempt is not a chanop on the specified channel. 
	/// </summary>
	/// <format>:You're not the original channel operator</format>
	ERR_UNIQOPPRIVSNEEDED = 485,

	/// <summary>
	/// Indicates that an <see cref="IrcCommand.OPER"/> command failed because the server has not
	/// been configured to allow connections from this client's host to become an operator.
	/// </summary>
	/// <format>(client) :No O-lines for your host</format>
	ERR_NOOPERHOST = 491,

	/// <summary>
	/// Indicates that a <see cref="IrcCommand.MODE_USER"/> command
	/// contained a MODE letter that was not recognized.
	/// </summary>
	/// <format>(client) :Unknown MODE flag</format>
	ERR_UMODEUNKNOWNFLAG = 501,

	/// <summary>
	/// Indicates that a <see cref="IrcCommand.MODE_USER"/> command failed
	/// because they were trying to set or view modes for other users.
	/// </summary>
	/// <format>(client) :Cant change mode for other users</format>
	ERR_USERSDONTMATCH = 502,

	/// <summary>
	/// Indicates that a <see cref="IrcCommand.HELP"/> command requested help on a subject the
	/// server does not know about. The (subject) must be the one requested by the client, but
	/// may be casefolded; unless it would be an invalid parameter, in which case it must be '*'.
	/// </summary>
	/// <format>(client) (subject) :No help available on this topic</format>
	ERR_HELPNOTFOUND = 524,

	/// <summary>
	/// Indicates the value of a key channel mode change (+k) was rejected.
	/// </summary>
	/// <remarks>
	/// Not to be confused with <see cref="ERR_BADCHANNELKEY"/>,
	/// which is returned when someone tries to join a channel.
	/// </remarks>
	/// <format>(client) (target chan) :Key is not well-formed</format>
	ERR_INVALIDKEY = 525,

	/// <summary>
	/// This numeric is used by the IRCv3 tls extension and indicates that the client may begin a TLS handshake.
	/// </summary>
	/// <format>(client) :STARTTLS successful, proceed with TLS handshake</format>
	[Obsolete("The tls extension has been deprecated in favour of implicit TLS and Strict Transport Security.")]
	RPL_STARTTLS = 670,

	/// <summary>
	/// Sent as a reply to the <see cref="IrcCommand.WHOIS"/> command, this numeric shows the client is
	/// connecting to the server in a way the server considers reasonably safe from eavesdropping.
	/// </summary>
	/// <format>(client) (nick) :is using a secure connection</format>
	RPL_WHOISSECURE = 671,

	/// <summary>
	/// This numeric is used by the IRCv3 tls extension and indicates that a server-side
	/// error occured and the STARTTLS command failed.
	/// </summary>
	/// <format>(client) :STARTTLS failed (Wrong moon phase)</format>
	[Obsolete("The tls extension has been deprecated in favour of implicit TLS and Strict Transport Security.")]
	ERR_STARTTLS = 691,

	/// <summary>
	/// Indicates that there was a problem with a mode parameter. Replaces
	/// various implementation-specific mode-specific numerics.
	/// </summary>
	/// <format>(client) (target chan/user) (mode char) (parameter) :(description)</format>
	ERR_INVALIDMODEPARAM = 696,

	/// <summary>
	/// Indicates the start of a reply to a <see cref="IrcCommand.HELP"/> command. The
	/// (subject) must be the one requested by the client, but may be casefolded;
	/// unless it would be an invalid parameter, in which case it must be '*'.
	/// </summary>
	/// <format>(client) (subject) :(first line of help section)</format>
	RPL_HELPSTART = 704,

	/// <summary>
	/// Returns a line of <see cref="IrcCommand.HELP"/> text to the client. Lines may be wrapped to a
	/// certain line length by the server. Note that the final line must be a <see cref="RPL_ENDOFHELP"/>
	/// numeric. The (subject) must be the one requested by the client, but may be casefolded;
	/// unless it would be an invalid parameter, in which case it must be '*'.
	/// </summary>
	/// <format>(client) (subject) :(line of help text)</format>
	RPL_HELPTXT = 705,

	/// <summary>
	/// Returns the final <see cref="IrcCommand.HELP"/> line to the client. The (subject)
	/// must be the one requested by the client, but may be casefolded; unless it
	/// would be an invalid parameter, in which case it must be '*'.
	/// </summary>
	/// <format>(client) (subject) :(last line of help text)</format>
	RPL_ENDOFHELP = 706,

	/// <summary>
	/// Sent by a server to alert an IRC operator that they they do not have the specific operator
	/// privilege required by this server/network to perform the command or action they requested.
	/// </summary>
	/// <format>(client) (priv) :Insufficient oper privileges.</format>
	ERR_NOPRIVS = 723,

	/// <summary>
	/// This numeric is used to indicate to a client that either a target has just become online,
	/// or that a target they have added to their monitor list is online.
	/// </summary>
	/// <remarks>The server may send a hostmask with the target. The server may send '*' instead of (nick).</remarks>
	/// <format>(client) :target[!user@host]{,target[!user@host]}</format>
	RPL_MONONLINE = 730,

	/// <summary>
	/// This numeric is used to indicate to a client that either a target has just left the
	/// irc network, or that a target they have added to their monitor list is offline.
	/// The argument is a chained list of targets that are offline.
	/// </summary>
	/// <remarks>The server may send '*' instead of (nick).</remarks>
	/// <format>(client) :target{,target2}</format>
	RPL_MONOFFLINE = 731,

	/// <summary>
	/// This numeric is used to indicate to a client the list of targets they have in their monitor list.
	/// </summary>
	/// <format>(client) :target{,target2}</format>
	RPL_MONLIST = 732,

	/// <summary>
	/// This numeric is used to indicate to a client the end of a monitor list.
	/// </summary>
	/// <format>(client) :End of MONITOR list</format>
	RPL_ENDOFMONLIST = 733,

	/// <summary>
	/// This numeric is used to indicate to a client that their monitor list is full, so the command failed.
	/// The (limit) parameter is the maximum number of targets a client may have in their list,
	/// the (targets) parameter is the list of targets, as the client sent them, that cannot be added.
	/// </summary>
	/// <format>(client) (limit) (targets) :Monitor list is full</format>
	ERR_MONLISTFULL = 734,

	/// <summary>
	/// This numeric indicates that the client was logged into the specified
	/// account (whether by SASL authentication or otherwise).
	/// </summary>
	/// <format>(client) (nick)!(user)@(host) (account) :You are now logged in as (username)</format>
	RPL_LOGGEDIN = 900,

	/// <summary>
	/// This numeric indicates that the client was logged out of their account.
	/// </summary>
	/// <format>(client) (nick)!(user)@(host) :You are now logged out</format>
	RPL_LOGGEDOUT = 901,

	/// <summary>
	/// This numeric indicates that SASL authentication failed because the account is
	/// currently locked out, held, or otherwise administratively made unavailable.
	/// </summary>
	/// <format>(client) :You must use a nick assigned to you</format>
	ERR_NICKLOCKED = 902,

	/// <summary>
	/// This numeric indicates that SASL authentication was completed successfully,
	/// and is normally sent along with <see cref="RPL_LOGGEDIN"/>.
	/// </summary>
	/// <format>(client) :SASL authentication successful</format>
	RPL_SASLSUCCESS = 903,

	/// <summary>
	/// This numeric indicates that SASL authentication failed because of invalid
	/// credentials or other errors not explicitly mentioned by other numerics.
	/// </summary>
	/// <format>(client) :SASL authentication failed</format>
	ERR_SASLFAIL = 904,

	/// <summary>
	/// This numeric indicates that SASL authentication failed because the
	/// <see cref="IrcCommand.AUTHENTICATE"/> command sent by the client was too long.
	/// </summary>
	/// <format>(client) :SASL message too long</format>
	ERR_SASLTOOLONG = 905,

	/// <summary>
	/// This numeric indicates that SASL authentication failed because the client sent an
	/// <see cref="IrcCommand.AUTHENTICATE"/> command with the parameter '*'.
	/// </summary>
	/// <format>(client) :SASL authentication aborted</format>
	ERR_SASLABORTED = 906,

	/// <summary>
	/// This numeric indicates that SASL authentication failed because the client has already authenticated
	/// using SASL and reauthentication is not available or has been administratively disabled.
	/// </summary>
	/// <format>(client) :You have already authenticated using SASL</format>
	ERR_SASLALREADY = 907,

	/// <summary>
	/// This numeric specifies the mechanisms supported for SASL authentication.
	/// (mechanisms) is a list of SASL mechanisms, delimited by a comma.
	/// </summary>
	/// <format>(client) (mechanisms) :are available SASL mechanisms</format>
	RPL_SASLMECHS = 908,
}