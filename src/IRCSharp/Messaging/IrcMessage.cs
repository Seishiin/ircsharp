using System.Collections.Generic;

namespace IRCSharp.Messaging;

/// <summary>
/// Represents an IRC message according to the protocol specification. It consists of a collection of
/// <see cref="Tags"/>, a <see cref="Source"/>, and the <see cref="Command"/>. If the command
/// is unknown, it is treated as a <see cref="IrcCommand.RAW"/> command that consists of a collection of arguments.
/// Otherwise, if it is a default command or registered using <see cref="IrcConfig.RegisterCommand{T}"/>,
/// it will be parsed via <see cref="IIrcCommand.FromArgs"/>.
/// </summary>
public class IrcMessage
{
	/// <summary>
	/// Message <see cref="IrcTag"/>s are a mechanism for adding additional metadata on a per-message basis.
	/// This is achieved via an extension to the protocol message format, enabled via capability negotiation.
	/// </summary>
	public ISet<IrcTag> Tags { get; init; }

	/// <summary>
	/// Indicates the true origin of a message. If the <see cref="IrcSource"/> is missing
	/// from a message, it is assumed to have originated from the client/server on the
	/// other end of the connection the <see cref="IrcMessage"/> was received on.
	/// </summary>
	public IrcSource? Source { get; init; }

	/// <summary>
	/// Represents the <see cref="IrcCommand"/> of this message.
	/// </summary>
	public IrcCommand Command { get; }

	internal IrcMessage(IrcCommand command)
	{
		Tags = new HashSet<IrcTag>();
		Command = command;
	}

	/// <summary>
	/// Gets the likely intended place to respond to this message. If the type of the
	/// message is a <see cref="IrcCommand.PRIVMSG"/> or <see cref="IrcCommand.NOTICE"/>
	/// and the message is sent to a channel, the result will be that channel. In all
	/// other cases, it will be <see cref="IrcSource.Nick"/>.
	/// </summary>
	/// <returns>The target to respond to.</returns>
	public string? ResponseTarget()
	{
		return Command switch
		{
			IrcCommand.PRIVMSG ({ } target, _)
				when Utils.IsChannel(target) => target,

			IrcCommand.NOTICE ({ } target, _)
				when Utils.IsChannel(target) => target,

			_ => Source?.Nick,
		};
	}

	public override string ToString()
	{
		string result = string.Empty;

		if (Tags.Count > 0)
			result += $"@{string.Join(';', Tags)} ";

		if (Source != null)
			result += $"{Source} ";

		result += Command;

		return result;
	}
}