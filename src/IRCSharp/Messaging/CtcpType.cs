namespace IRCSharp.Messaging;

/// <summary>
/// Represents the type of a <see cref="IrcCommand.CTCP"/> command.
/// </summary>
public enum CtcpType : byte
{
	/// <summary>
	/// Used to send a message to a target. If there is a response, it will be a <see cref="Reply"/>.
	/// These CTCP messages are sent with <see cref="IrcCommand.PRIVMSG"/>.
	/// </summary>
	Query,

	/// <summary>
	/// Used to reply to a <see cref="Query"/>.
	/// These CTCP messages are sent with <see cref="IrcCommand.NOTICE"/>.
	/// </summary>
	Reply,
}