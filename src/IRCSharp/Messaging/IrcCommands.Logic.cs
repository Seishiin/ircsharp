// ReSharper disable InconsistentNaming

using System;
using System.Collections.Generic;
using System.Linq;
using IRCSharp.Modes;

namespace IRCSharp.Messaging;

public abstract partial record IrcCommand
{
	public partial record PASS
	{
		public static string Identifier() => "PASS";
		public override string ToCommandString() => Stringify<PASS>(password);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 1 ? new PASS(args[0]) : null;
	}

	public partial record NICK
	{
		public static string Identifier() => "NICK";
		public override string ToCommandString() => Stringify<NICK>(nickname);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 1 ? new NICK(args[0]) : null;
	}

	public partial record USER
	{
		public static string Identifier() => "USER";
		public override string ToCommandString() => Stringify<USER>(user, mode.ToString(), "*", realname);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			if (args.Count == 4 && byte.TryParse(args[1], out byte mode))
				return new USER(args[0], mode, args[3]);

			return null;
		}
	}

	public partial record OPER
	{
		public static string Identifier() => "OPER";
		public override string ToCommandString() => Stringify<OPER>(name, password);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 2 ? new OPER(args[0], args[1]) : null;
	}

	public partial record MODE_USER
	{
		public static string Identifier() => "MODE";
		public override string ToCommandString() => Stringify<MODE_USER>(nickname, modes?.ToString());

		public static IrcCommand? FromArgs(IList<string> args)
		{
			if (args.Count == 0) return null;

			if (Utils.IsChannel(args[0]))
			{
				return args.Count switch
				{
					1 => new MODE_CHANNEL(args[0]),
					2 => new MODE_CHANNEL(args[0], IrcModes.Parse(args[1])),
					_ => new MODE_CHANNEL(args[0], IrcModes.Parse(args[1]), args.Skip(2)),
				};
			}

			return args.Count switch
			{
				1 => new MODE_USER(args[0]),
				2 => new MODE_USER(args[0], IrcModes.Parse(args[1])),
				_ => null,
			};
		}
	}

	public partial record SERVICE
	{
		public static string Identifier() => "SERVICE";
		public override string ToCommandString() => Stringify<SERVICE>(nickname, "*", distribution, type, "0", info);

		public static IrcCommand? FromArgs(IList<string> args) =>
			args.Count == 6 ? new SERVICE(args[0], args[2], args[3], args[5]) : null;
	}

	public partial record QUIT
	{
		public static string Identifier() => "QUIT";
		public override string ToCommandString() => Stringify<QUIT>(comment);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new QUIT(),
				1 => new QUIT(args[0]),
				_ => null,
			};
		}
	}

	public partial record SQUIT
	{
		public static string Identifier() => "SQUIT";
		public override string ToCommandString() => Stringify<SQUIT>(server, comment);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 2 ? new SQUIT(args[0], args[1]) : null;
	}

	public partial record JOIN
	{
		public static string Identifier() => "JOIN";

		public override string ToCommandString() => leaveAll ? Stringify<JOIN>("0") :
			Stringify<JOIN>(channels.Merge(','), keys.Merge(','));

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 when args[0] == "0" => new JOIN(Array.Empty<string>(), leaveAll: true),
				1                     => new JOIN(args[0].Split(',')),
				2                     => new JOIN(args[0].Split(','), args[1].Split(',')),
				_                     => null,
			};
		}
	}

	public partial record PART
	{
		public static string Identifier() => "PART";
		public override string ToCommandString() => Stringify<PART>(channels.Merge(','), comment);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 => new PART(args[0].Split(',')),
				2 => new PART(args[0].Split(','), args[1]),
				_ => null,
			};
		}
	}

	public partial record MODE_CHANNEL
	{
		public static string Identifier() => "MODE";

		public override string ToCommandString() => Stringify<MODE_CHANNEL>(new[] { channel, modes?.ToString() }
		                                                                   .Concat(modeArgs ?? Array.Empty<string>())
		                                                                   .ToArray());

		public static IrcCommand? FromArgs(IList<string> args) => MODE_USER.FromArgs(args);
	}

	public partial record TOPIC
	{
		public static string Identifier() => "TOPIC";
		public override string ToCommandString() => Stringify<TOPIC>(channel, topic);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 => new TOPIC(args[0]),
				2 => new TOPIC(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record NAMES
	{
		public static string Identifier() => "NAMES";
		public override string ToCommandString() => Stringify<NAMES>(channels.Merge(','), server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new NAMES(),
				1 => new NAMES(args[0].Split(',')),
				2 => new NAMES(args[0].Split(','), args[1]),
				_ => null,
			};
		}
	}

	public partial record LIST
	{
		public static string Identifier() => "LIST";
		public override string ToCommandString() => Stringify<LIST>(channels.Merge(','), server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new LIST(),
				1 => new LIST(args[0].Split(',')),
				2 => new LIST(args[0].Split(','), args[1]),
				_ => null,
			};
		}
	}

	public partial record INVITE
	{
		public static string Identifier() => "INVITE";
		public override string ToCommandString() => Stringify<INVITE>(nickname, channel);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 2 ? new INVITE(args[0], args[1]) : null;
	}

	public partial record KICK
	{
		public static string Identifier() => "KICK";
		public override string ToCommandString() => Stringify<KICK>(channels.Merge(','), users.Merge(','), comment);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				2 => new KICK(args[0].Split(','), args[1].Split(',')),
				3 => new KICK(args[0].Split(','), args[1].Split(','), args[2]),
				_ => null,
			};
		}
	}

	public partial record PRIVMSG
	{
		public static string Identifier() => "PRIVMSG";
		public override string ToCommandString() => Stringify<PRIVMSG>(target, text);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			if (args.Count != 2) return null;

			var ctc = CTCP.Parse(CtcpType.Query, args[0], args[1]);
			if (ctc != null) return ctc;

			return new PRIVMSG(args[0], args[1]);
		}
	}

	public partial record NOTICE
	{
		public static string Identifier() => "NOTICE";
		public override string ToCommandString() => Stringify<NOTICE>(target, text);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			if (args.Count != 2) return null;

			var ctc = CTCP.Parse(CtcpType.Reply, args[0], args[1]);
			if (ctc != null) return ctc;

			return new NOTICE(args[0], args[1]);
		}
	}

	public partial record CTCP
	{
		private const char delim = '\x01';
		public static string Identifier() => "CTCP";

		public override string ToCommandString()
		{
			string s = string.IsNullOrEmpty(text) ? command : $"{command} {text}";
			return type == CtcpType.Query ?
				new PRIVMSG(target, $"{delim}{s}{delim}").ToString() :
				new NOTICE(target, $"{delim}{s}{delim}").ToString();
		}

		public static IrcCommand FromArgs(IList<string> args) => throw new NotSupportedException();

		internal static CTCP? Parse(CtcpType type, string target, string input)
		{
			if (IsCtcp(input, out string txt))
			{
				string[] split = txt.Split(' ', 2);
				return split.Length == 1 ?
					new CTCP(type, target, split[0]) :
					new CTCP(type, target, split[0], split[1]);
			}

			return null;
		}

		private static bool IsCtcp(string s, out string text)
		{
			bool isCtcp = s.StartsWith(delim);

			if (isCtcp)
			{
				int end = s.Length > 1 && s.EndsWith(delim) ? s.Length - 1 : s.Length;
				text = s[1..end];
			}
			else
			{
				text = string.Empty;
			}

			return isCtcp;
		}
	}

	public partial record MOTD
	{
		public static string Identifier() => "MOTD";
		public override string ToCommandString() => Stringify<MOTD>(server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new MOTD(),
				1 => new MOTD(args[0]),
				_ => null,
			};
		}
	}

	public partial record LUSERS
	{
		public static string Identifier() => "LUSERS";
		public override string ToCommandString() => Stringify<LUSERS>(mask, server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new LUSERS(),
				1 => new LUSERS(args[0]),
				2 => new LUSERS(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record VERSION
	{
		public static string Identifier() => "VERSION";
		public override string ToCommandString() => Stringify<VERSION>(target);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new VERSION(),
				1 => new VERSION(args[0]),
				_ => null,
			};
		}
	}

	public partial record STATS
	{
		public static string Identifier() => "STATS";
		public override string ToCommandString() => Stringify<STATS>(query, server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new STATS(),
				1 => new STATS(args[0]),
				2 => new STATS(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record LINKS
	{
		public static string Identifier() => "LINKS";
		public override string ToCommandString() => Stringify<LINKS>(remoteserver, servermask);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new LINKS(),
				1 => new LINKS(args[0]),
				2 => new LINKS(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record TIME
	{
		public static string Identifier() => "TIME";
		public override string ToCommandString() => Stringify<TIME>(server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new TIME(),
				1 => new TIME(args[0]),
				_ => null,
			};
		}
	}

	public partial record CONNECT
	{
		public static string Identifier() => "CONNECT";
		public override string ToCommandString() => Stringify<CONNECT>(targetserver, port, remoteserver);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				2 => new CONNECT(args[0], args[1]),
				3 => new CONNECT(args[0], args[1], args[2]),
				_ => null,
			};
		}
	}

	public partial record TRACE
	{
		public static string Identifier() => "TRACE";
		public override string ToCommandString() => Stringify<TRACE>(target);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new TRACE(),
				1 => new TRACE(args[0]),
				_ => null,
			};
		}
	}

	public partial record ADMIN
	{
		public static string Identifier() => "ADMIN";
		public override string ToCommandString() => Stringify<ADMIN>(target);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new ADMIN(),
				1 => new ADMIN(args[0]),
				_ => null,
			};
		}
	}

	public partial record INFO
	{
		public static string Identifier() => "INFO";
		public override string ToCommandString() => Stringify<INFO>(server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new INFO(),
				1 => new INFO(args[0]),
				_ => null,
			};
		}
	}

	public partial record HELP
	{
		public static string Identifier() => "HELP";
		public override string ToCommandString() => Stringify<INFO>(subject);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new HELP(),
				1 => new HELP(args[0]),
				_ => null,
			};
		}
	}

	public partial record SERVLIST
	{
		public static string Identifier() => "SERVLIST";
		public override string ToCommandString() => Stringify<SERVLIST>(mask, type);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new SERVLIST(),
				1 => new SERVLIST(args[0]),
				2 => new SERVLIST(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record SQUERY
	{
		public static string Identifier() => "SQUERY";
		public override string ToCommandString() => Stringify<SQUERY>(servicename, text);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 2 ? new SQUERY(args[0], args[1]) : null;
	}

	public partial record WHO
	{
		public static string Identifier() => "WHO";
		public override string ToCommandString() => Stringify<WHO>(mask, ops ? "o" : "");

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new WHO(),
				1 => new WHO(args[0]),
				2 => new WHO(args[0], args[1] == "o"),
				_ => null,
			};
		}
	}

	public partial record WHOIS
	{
		public static string Identifier() => "WHOIS";
		public override string ToCommandString() => Stringify<WHOIS>(target, masks.Merge(','));

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 => new WHOIS(args[0].Split(',')),
				2 => new WHOIS(args[1].Split(','), args[0]), //target is first
				_ => null,
			};
		}
	}

	public partial record WHOWAS
	{
		public static string Identifier() => "WHOWAS";
		public override string ToCommandString() => Stringify<WHOWAS>(nicknames.Merge(','), count.ToString(), target);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1                                           => new WHOWAS(args[0].Split(',')),
				2 when int.TryParse(args[1], out int count) => new WHOWAS(args[0].Split(','), count),
				3 when int.TryParse(args[1], out int count) => new WHOWAS(args[0].Split(','), count, args[2]),
				_                                           => null,
			};
		}
	}

	public partial record KILL
	{
		public static string Identifier() => "KILL";
		public override string ToCommandString() => Stringify<KILL>(nickname, comment);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 2 ? new KILL(args[0], args[1]) : null;
	}

	public partial record PING
	{
		public static string Identifier() => "PING";
		public override string ToCommandString() => Stringify<PING>(server1, server2);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 => new PING(args[0]),
				2 => new PING(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record PONG
	{
		public static string Identifier() => "PONG";
		public override string ToCommandString() => Stringify<PONG>(server, server2);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 => new PONG(args[0]),
				2 => new PONG(args[0], args[1]),
				_ => null,
			};
		}
	}

	public partial record ERROR
	{
		public static string Identifier() => "ERROR";
		public override string ToCommandString() => Stringify<ERROR>(message);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 1 ? new ERROR(args[0]) : null;
	}

	public partial record AWAY
	{
		public static string Identifier() => "AWAY";
		public override string ToCommandString() => Stringify<AWAY>(comment);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new AWAY(),
				1 => new AWAY(args[0]),
				_ => null,
			};
		}
	}

	public partial record REHASH
	{
		public static string Identifier() => "REHASH";
		public override string ToCommandString() => Stringify<REHASH>();
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 0 ? new REHASH() : null;
	}

	public partial record DIE
	{
		public static string Identifier() => "DIE";
		public override string ToCommandString() => Stringify<DIE>();
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 0 ? new DIE() : null;
	}

	public partial record RESTART
	{
		public static string Identifier() => "RESTART";
		public override string ToCommandString() => Stringify<RESTART>();
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 0 ? new RESTART() : null;
	}

	public partial record SUMMON
	{
		public static string Identifier() => "SUMMON";
		public override string ToCommandString() => Stringify<SUMMON>(user, server, channel);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				1 => new SUMMON(args[0]),
				2 => new SUMMON(args[0], args[1]),
				3 => new SUMMON(args[0], args[1], args[2]),
				_ => null,
			};
		}
	}

	public partial record USERS
	{
		public static string Identifier() => "USERS";
		public override string ToCommandString() => Stringify<USERS>(server);

		public static IrcCommand? FromArgs(IList<string> args)
		{
			return args.Count switch
			{
				0 => new USERS(),
				1 => new USERS(args[0]),
				_ => null,
			};
		}
	}

	public partial record WALLOPS
	{
		public static string Identifier() => "WALLOPS";
		public override string ToCommandString() => Stringify<WALLOPS>(text);
		public static IrcCommand? FromArgs(IList<string> args) => args.Count == 1 ? new WALLOPS(args[0]) : null;
	}

	public partial record USERHOST
	{
		public static string Identifier() => "USERHOST";
		public override string ToCommandString() => Stringify<USERHOST>(nicknames.Merge(' '));
		public static IrcCommand? FromArgs(IList<string> args) => args.Count > 0 ? new USERHOST(args) : null;
	}

	public partial record ISON
	{
		public static string Identifier() => "ISON";
		public override string ToCommandString() => Stringify<ISON>(nicknames.Merge(' '));
		public static IrcCommand? FromArgs(IList<string> args) => args.Count > 0 ? new ISON(args) : null;
	}

	public partial record RESPONSE
	{
		public static string Identifier() => "RESPONSE";
		public override string ToCommandString() => Stringify($"{(int)numeric:000}", args.ToArray());
		public static IrcCommand FromArgs(IList<string> args) => throw new NotSupportedException();

		internal static RESPONSE? Parse(string id, IList<string> args)
		{
			if (ushort.TryParse(id, out ushort num))
			{
				var code = (IrcNumericReply)num;
				if (Enum.IsDefined(code))
					return new RESPONSE(code, args);
			}

			return null;
		}
	}

	public partial record RAW
	{
		public static string Identifier() => "RAW";
		public override string ToCommandString() => Stringify(name, args.ToArray());
		public static IrcCommand FromArgs(IList<string> args) => throw new NotSupportedException();
	}
}