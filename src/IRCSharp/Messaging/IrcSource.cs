using System;

namespace IRCSharp.Messaging;

/// <summary>
/// The source (formerly known as prefix) indicates the true origin of a message
/// and consists of a <see cref="Nick"/>, <see cref="User"/> and <see cref="Host"/>.
/// </summary>
public class IrcSource
{
	/// <summary>
	/// This is the unique nickname, server name or service name. If it is a
	/// server name, <see cref="User"/> and <see cref="Host"/> should be empty.
	/// </summary>
	public string Nick { get; }

	/// <summary>
	/// This is the username. Servers may use the Ident Protocol to look up the "real username" of clients.
	/// If username lookups are enabled and a client does not have an Identity Server enabled, the username
	/// provided by the client should be prefixed by a tilde to show that this value is user-set.
	/// </summary>
	public string? User { get; init; }

	/// <summary>
	/// This is the hostname or IP address of the user/service.
	/// Is not accurate due to how IRC servers can spoof hostnames.
	/// </summary>
	public string? Host { get; init; }

	/// <summary>
	/// Returns true if this <see cref="IrcSource"/> looks like a server name.
	/// </summary>
	public bool IsServer => Nick.Contains('.') &&
	                        string.IsNullOrEmpty(User) &&
	                        string.IsNullOrEmpty(Host);

	public IrcSource(string nick)
	{
		Nick = nick;
	}

	/// <summary>
	/// Parses a span of characters into an <see cref="IrcSource"/>.
	/// </summary>
	/// <param name="input">A span containing the characters representing the <see cref="IrcSource"/> to convert.</param>
	/// <returns>An <see cref="IrcSource"/>.</returns>
	public static IrcSource Parse(ReadOnlySpan<char> input)
	{
		int nu = input.IndexOf('!');
		int nh = input.IndexOf('@');

		string nick;
		string? user = null;
		string? host = null;

		if (nu == -1 && nh == -1)
		{
			nick = input.ToString();
		}
		else if (nh == -1)
		{
			nick = input[..nu].ToString();
			user = input[(nu + 1)..].ToString();
		}
		else if (nu == -1)
		{
			nick = input[..nh].ToString();
			host = input[(nh + 1)..].ToString();
		}
		else
		{
			nick = input[..nu].ToString();
			user = input[(nu + 1)..nh].ToString();
			host = input[(nh + 1)..].ToString();
		}

		return new IrcSource(nick) { User = user, Host = host };
	}

	public override string ToString()
	{
		string user = string.IsNullOrEmpty(User) ? "" : $"!{User}";
		string host = string.IsNullOrEmpty(Host) ? "" : $"@{Host}";
		return $":{Nick}{user}{host}";
	}
}