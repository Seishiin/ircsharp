// ReSharper disable InconsistentNaming

using System.Collections.Generic;
using System.Linq;
using IRCSharp.Modes;

namespace IRCSharp.Messaging;

/// <summary>
/// Defines methods to identify and construct <see cref="IrcCommand"/>s.
/// </summary>
public interface IIrcCommand
{
	/// <summary>
	/// Used to uniquely identify a command.
	/// </summary>
	/// <returns>The name of the command.</returns>
	static abstract string Identifier();

	/// <summary>
	/// Used to construct an <see cref="IrcCommand"/> from the given arguments.
	/// If the arguments are not valid, null should be returned.
	/// </summary>
	/// <param name="args">The arguments to construct the command from.</param>
	/// <returns>An <see cref="IrcCommand"/>.</returns>
	static abstract IrcCommand? FromArgs(IList<string> args);
}

/// <summary>
/// The base class of all IRC commands. If you want to create a custom command,
/// you have to inherit from this class and implement <see cref="IIrcCommand"/>.
/// </summary>
public abstract partial record IrcCommand
{
	public sealed override string ToString() => ToCommandString();

	/// <summary>
	/// Returns a string that represents the current command. It is recommended
	/// to use <see cref="Stringify{T}()"/> or <see cref="Stringify{T}(string[])"/>
	/// to create that string.
	/// </summary>
	/// <returns>A string that represents the current command.</returns>
	public abstract string ToCommandString();

	/// <summary>
	/// Returns the name of the command. Should be used for commands that have no arguments.
	/// </summary>
	/// <typeparam name="T">An <see cref="IIrcCommand"/>.</typeparam>
	/// <returns>A string that represents <see cref="T"/>.</returns>
	protected static string Stringify<T>() where T : IIrcCommand => T.Identifier();

	/// <summary>
	/// Returns the name of the command, concatenates the arguments and optionally prefixes the
	/// trailing argument with a colon if necessary. Should be used for commands that have arguments.
	/// </summary>
	/// <param name="args">The arguments to concatenate.</param>
	/// <typeparam name="T">An <see cref="IIrcCommand"/>.</typeparam>
	/// <returns>A string that represents <see cref="T"/>.</returns>
	protected static string Stringify<T>(params string?[]? args) where T : IIrcCommand => Stringify(T.Identifier(), args);

	private static string Stringify(string id, params string?[]? args)
	{
		if (args == null) return $"{id} :";

		string[] valid = args.Where(s => s != null).ToArray()!;
		if (valid.Length == 0) return id;

		string suffix = valid[^1];
		string middle = string.Join(' ', valid[..^1]);
		string space = middle.Length == 0 ? "" : " ";
		string colon = suffix.Length == 0 || suffix.StartsWith(':') || suffix.Contains(' ') ? ":" : "";
		string delim = space.Length > 0 || colon.Length > 0 || suffix.Length > 0 ? " " : "";

		return $"{id}{space}{middle}{delim}{colon}{suffix}";
	}
}

public abstract partial record IrcCommand
{
	#region Connection Registration
	/// <summary>
	/// This command is used to set a "connection password".
	/// </summary>
	/// <format>PASS (password)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_ALREADYREGISTRED"/><br/>
	/// <see cref="IrcNumericReply.ERR_PASSWDMISMATCH"/><br/>
	/// </numerics>
	public sealed partial record PASS(string password) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to give the client a (nickname) or change the previous one.
	/// </summary>
	/// <format>NICK (nickname)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NONICKNAMEGIVEN"/><br/>
	/// <see cref="IrcNumericReply.ERR_ERRONEUSNICKNAME"/><br/>
	/// <see cref="IrcNumericReply.ERR_NICKNAMEINUSE"/><br/>
	/// <see cref="IrcNumericReply.ERR_NICKCOLLISION"/><br/>
	/// <see cref="IrcNumericReply.ERR_UNAVAILRESOURCE"/><br/>
	/// <see cref="IrcNumericReply.ERR_RESTRICTED"/><br/>
	/// </numerics>
	public sealed partial record NICK(string nickname) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used at the beginning of a connection to specify the (user) and (realname) of a new user.
	/// </summary>
	/// <format>USER (user) (mode) * (realname)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_ALREADYREGISTRED"/><br/>
	/// </numerics>
	public sealed partial record USER(string user, byte mode, string realname) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used by a normal user to obtain IRC operator privileges.
	/// </summary>
	/// <format>OPER (name) (password)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_PASSWDMISMATCH"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOOPERHOST"/><br/>
	/// <see cref="IrcNumericReply.RPL_YOUREOPER"/><br/>
	/// </numerics>
	public sealed partial record OPER(string name, string password) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to set or remove <see cref="IrcModes"/> for a given user. If no (modes)
	/// are specified, the server will return the current settings for the (nickname).
	/// </summary>
	/// <format>MODE (nickname) [(modes)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_UMODEUNKNOWNFLAG"/><br/>
	/// <see cref="IrcNumericReply.ERR_USERSDONTMATCH"/><br/>
	/// <see cref="IrcNumericReply.RPL_UMODEIS"/><br/>
	/// </numerics>
	public sealed partial record MODE_USER(string nickname, IrcModes? modes = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to register a new service. (distribution) is used to specify
	/// the visibility of a service. (type) is currently reserved for future usage.
	/// </summary>
	/// <format>SERVICE (nickname) * (distribution) (type) 0 (info)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_ALREADYREGISTRED"/><br/>
	/// <see cref="IrcNumericReply.ERR_ERRONEUSNICKNAME"/><br/>
	/// <see cref="IrcNumericReply.RPL_YOURESERVICE"/><br/>
	/// <see cref="IrcNumericReply.RPL_YOURHOST"/><br/>
	/// <see cref="IrcNumericReply.RPL_MYINFO"/><br/>
	/// </numerics>
	public sealed partial record SERVICE(string nickname, string distribution, string type, string info) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to terminate a client's connection to the server. (comment)
	/// is the reason why the client has terminated the connection to the server.
	/// </summary>
	/// <format>QUIT [(comment)]</format>
	/// <numerics>None</numerics>
	public sealed partial record QUIT(string? comment = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command disconnects a (server) from the network. This is a privileged command and is only
	/// available to IRC Operators. (comment) is the reason why the (server) link is being disconnected.
	/// </summary>
	/// <format>SQUIT (server) (comment)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// </numerics>
	public sealed partial record SQUIT(string server, string comment) : IrcCommand, IIrcCommand;
	#endregion

	#region Channel operations
	/// <summary>
	/// This command indicates that the client wants to join the given (channels), each (channel)
	/// using the given (key) for it. Specifying (leaveAll) instead of the usual parameters
	/// requests the sending client to leave all (channels) it is currently connected to.
	/// </summary>
	/// <format>JOIN ((channels) [(keys)]) | 0</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_BANNEDFROMCHAN"/><br/>
	/// <see cref="IrcNumericReply.ERR_INVITEONLYCHAN"/><br/>
	/// <see cref="IrcNumericReply.ERR_BADCHANNELKEY"/><br/>
	/// <see cref="IrcNumericReply.ERR_CHANNELISFULL"/><br/>
	/// <see cref="IrcNumericReply.ERR_BADCHANMASK"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_TOOMANYCHANNELS"/><br/>
	/// <see cref="IrcNumericReply.ERR_TOOMANYTARGETS"/><br/>
	/// <see cref="IrcNumericReply.ERR_UNAVAILRESOURCE"/><br/>
	/// <see cref="IrcNumericReply.RPL_TOPIC"/><br/>
	/// <see cref="IrcNumericReply.RPL_TOPICWHOTIME"/><br/>
	/// <see cref="IrcNumericReply.RPL_NAMREPLY"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFNAMES"/><br/>
	/// </numerics>
	public sealed partial record JOIN(IEnumerable<string> channels, IEnumerable<string>? keys = null, bool leaveAll = false) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command removes the client from the given (channels). (comment)
	/// is the reason why the client has left the (channels).
	/// </summary>
	/// <format>PART (channels) [(comment)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTONCHANNEL"/><br/>
	/// </numerics>
	public sealed partial record PART(IEnumerable<string> channels, string? comment = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to set or remove <see cref="IrcModes"/> for a given (channel). If no (modes)
	/// are specified, the server will return the current settings for the (channel).
	/// </summary>
	/// <format>MODE (channel) [[(modes)] (modeArgs)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_KEYSET"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOCHANMODES"/><br/>
	/// <see cref="IrcNumericReply.ERR_CHANOPRIVSNEEDED"/><br/>
	/// <see cref="IrcNumericReply.ERR_USERNOTINCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_UNKNOWNMODE"/><br/>
	/// <see cref="IrcNumericReply.RPL_CHANNELMODEIS"/><br/>
	/// <see cref="IrcNumericReply.RPL_BANLIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFBANLIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_EXCEPTLIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFEXCEPTLIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_INVITELIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFINVITELIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_UNIQOPIS"/><br/>
	/// </numerics>
	public sealed partial record MODE_CHANNEL(string channel, IrcModes? modes = null, IEnumerable<string>? modeArgs = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to change or view the topic of the given (channel). If (topic) is
	/// not specified, the (channel)'s current topic or lack thereof is returned. If (topic) is
	/// an empty string, the (topic) for the (channel) will be cleared.
	/// </summary>
	/// <format>TOPIC (channel) [(topic)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTONCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_CHANOPRIVSNEEDED"/><br/>
	/// <see cref="IrcNumericReply.RPL_NOTOPIC"/><br/>
	/// <see cref="IrcNumericReply.RPL_TOPIC"/><br/>
	/// <see cref="IrcNumericReply.RPL_TOPICWHOTIME"/><br/>
	/// </numerics>
	public sealed partial record TOPIC(string channel, string? topic = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to view the nicknames joined to a (channel) and their (channel) membership prefixes.
	/// If (server) is specified, the request is forwarded to that (server) which will generate the reply.
	/// </summary>
	/// <format>NAMES [(channels) [(server)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_NAMREPLY"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFNAMES"/><br/>
	/// </numerics>
	public sealed partial record NAMES(IEnumerable<string>? channels = null, string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to get a list of (channels) along with some information about each (channel).
	/// If (server) is specified, the request is forwarded to that (server) which will generate the reply.
	/// </summary>
	/// <format>LIST [(channels) [(server)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_LISTSTART"/><br/>
	/// <see cref="IrcNumericReply.RPL_LIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_LISTEND"/><br/>
	/// </numerics>
	public sealed partial record LIST(IEnumerable<string>? channels = null, string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to invite a user to a (channel).
	/// </summary>
	/// <format>INVITE (nickname) (channel)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTONCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_USERONCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_CHANOPRIVSNEEDED"/><br/>
	/// <see cref="IrcNumericReply.RPL_INVITING"/><br/>
	/// <see cref="IrcNumericReply.RPL_AWAY"/><br/>
	/// </numerics>
	public sealed partial record INVITE(string nickname, string channel) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command can be used to request the forced removal of a (user) from a (channel).
	/// It causes the (user) to be removed from the (channel) by force.
	/// </summary>
	/// <format>KICK (channels) (users) [(comment)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_BADCHANMASK"/><br/>
	/// <see cref="IrcNumericReply.ERR_CHANOPRIVSNEEDED"/><br/>
	/// <see cref="IrcNumericReply.ERR_USERNOTINCHANNEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTONCHANNEL"/><br/>
	/// </numerics>
	public sealed partial record KICK(IEnumerable<string> channels, IEnumerable<string> users, string? comment = null) : IrcCommand, IIrcCommand;
	#endregion

	#region Sending messages
	/// <summary>
	/// This command is used to send private messages between users, as well as to send messages
	/// to channels. (target) is the nickname of a client or the name of a channel.
	/// </summary>
	/// <format>PRIVMSG (target) (text)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NORECIPIENT"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTEXTTOSEND"/><br/>
	/// <see cref="IrcNumericReply.ERR_CANNOTSENDTOCHAN"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTOPLEVEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_WILDTOPLEVEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_TOOMANYTARGETS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_AWAY"/><br/>
	/// </numerics>
	public sealed partial record PRIVMSG(string target, string text) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to send notices between users, as well as to send notices to channels.
	/// (target) is interpreted the same way as it is for the <see cref="IrcCommand.PRIVMSG"/> command.
	/// This command is used similarly to <see cref="IrcCommand.PRIVMSG"/>. The difference is that
	/// automatic replies must never be sent in response to a <see cref="IrcCommand.NOTICE"/> message.
	/// </summary>
	/// <format>NOTICE (target) (text)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NORECIPIENT"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTEXTTOSEND"/><br/>
	/// <see cref="IrcNumericReply.ERR_CANNOTSENDTOCHAN"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTOPLEVEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_WILDTOPLEVEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_TOOMANYTARGETS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_AWAY"/><br/>
	/// </numerics>
	public sealed partial record NOTICE(string target, string text) : IrcCommand, IIrcCommand;

	/// <summary>
	/// CTCP extends the original IRC protocol by allowing users to query other clients
	/// or channels, this causes all the clients in the channel to reply the CTCP, for
	/// specific information. Additionally, CTCP can be used to encode messages that
	/// the raw IRC protocol would not allow to be sent over the link. (type) can be
	/// either a <see cref="CtcpType.Query"/> or a <see cref="CtcpType.Reply"/>.
	/// (target) is the nickname of a client or the name of a channel, (command) is
	/// the CTCP command (e.g. VERSION), and (text) is additional information to be
	/// sent to the (target). 
	/// </summary>
	/// <format>PRIVMSG | NOTICE (target) '\0x01'(command) (arguments)'\0x01'</format>
	public sealed partial record CTCP(CtcpType type, string target, string command, string? text = null) : IrcCommand, IIrcCommand;
	#endregion

	#region Server queries and commands
	/// <summary>
	/// This command is used to get the "Message of the Day" of the given (server). If (server) is
	/// not specified, the MOTD of the (server) the client is connected to should be returned.
	/// </summary>
	/// <format>MOTD [(server)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOMOTD"/><br/>
	/// <see cref="IrcNumericReply.RPL_MOTDSTART"/><br/>
	/// <see cref="IrcNumericReply.RPL_MOTD"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFMOTD"/><br/>
	/// </numerics>
	public sealed partial record MOTD(string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command returns statistics about local and global users. If (mask) is specified,
	/// the reply will only concern the part of the network formed by the servers matching the
	/// mask. If (server) is specified, the request is forwarded to that (server) which will
	/// generate the reply.
	/// </summary>
	/// <format>LUSERS [(mask) [(server)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_LUSERCLIENT"/><br/>
	/// <see cref="IrcNumericReply.RPL_LUSEROP"/><br/>
	/// <see cref="IrcNumericReply.RPL_LUSERUNKNOWN"/><br/>
	/// <see cref="IrcNumericReply.RPL_LUSERCHANNELS"/><br/>
	/// <see cref="IrcNumericReply.RPL_LUSERME"/><br/>
	/// <see cref="IrcNumericReply.RPL_LOCALUSERS"/><br/>
	/// <see cref="IrcNumericReply.RPL_GLOBALUSERS"/><br/>
	/// </numerics>
	public sealed partial record LUSERS(string? mask = null, string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to query the version of the software and the <see cref="IrcNumericReply.RPL_ISUPPORT"/>
	/// parameters of the given server. If (target) is not specified, the information for the server the client is
	/// connected to should be returned. If (target) is a server, the information for that server is requested. If
	/// (target) is a client, the information for the server that client is connected to is requested.
	/// </summary>
	/// <format>VERSION [(target)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_ISUPPORT"/><br/>
	/// <see cref="IrcNumericReply.RPL_VERSION"/><br/>
	/// </numerics>
	public sealed partial record VERSION(string? target = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to (query) statistics of a certain (server). The specific
	/// (queries) supported by this command depend on the server that replies.
	/// </summary>
	/// <format>STATS [(query) [(server)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVS"/><br/>
	/// <see cref="IrcNumericReply.RPL_STATSOLINE"/><br/>
	/// <see cref="IrcNumericReply.RPL_STATSLINKINFO"/><br/>
	/// <see cref="IrcNumericReply.RPL_STATSUPTIME"/><br/>
	/// <see cref="IrcNumericReply.RPL_STATSCOMMANDS"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFSTATS"/><br/>
	/// </numerics>
	public sealed partial record STATS(string? query = null, string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to list all servers which are known by the server answering the query,
	/// usually including the server itself. If (remoteserver) is specified in addition to (servermask),
	/// the command is forwarded to the first server found that matches that name, and that server is
	/// then required to answer the query.
	/// </summary>
	/// <format>LINKS [[(remoteserver)] :(servermask)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_LINKS"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFLINKS"/><br/>
	/// </numerics>
	public sealed partial record LINKS(string? remoteserver = null, string? servermask = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to query local time from the specified server. If (server)
	/// is not specified, the (server) handling the command must reply to the query.
	/// </summary>
	/// <format>TIME [(server)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_TIME"/><br/>
	/// </numerics>
	public sealed partial record TIME(string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command forces a server to try to establish a new connection to another server. This command
	/// is a privileged command and is available only to IRC Operators. If (remoteserver) is specified,
	/// the connection is attempted by that (remoteserver) to (targetserver) using (port).
	/// </summary>
	/// <format>CONNECT (targetserver) (port) [(remoteserver)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVS"/><br/>
	/// </numerics>
	public sealed partial record CONNECT(string targetserver, string port, string? remoteserver = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to find the route to a specific server and information about its peers.
	/// </summary>
	/// <format>TRACE [(target)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACELINK"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACECONNECTING"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACEHANDSHAKE"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACEUNKNOWN"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACEOPERATOR"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACEUSER"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACESERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACESERVICE"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACENEWTYPE"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACECLASS"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACELOG"/><br/>
	/// <see cref="IrcNumericReply.RPL_TRACEEND"/><br/>
	/// </numerics>
	public sealed partial record TRACE(string? target = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to find the name of the administrator of the given server. If (target)
	/// is not specified, the information for the server the client is connected to should be returned.
	/// If (target) is a server, the information for that server is requested. If (target) is a client,
	/// the information for the server that client is connected to is requested.
	/// </summary>
	/// <format>ADMIN [(target)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_ADMINME"/><br/>
	/// <see cref="IrcNumericReply.RPL_ADMINLOC1"/><br/>
	/// <see cref="IrcNumericReply.RPL_ADMINLOC2"/><br/>
	/// <see cref="IrcNumericReply.RPL_ADMINEMAIL"/><br/>
	/// </numerics>
	public sealed partial record ADMIN(string? target = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to return information which describes the (server). This information
	/// usually includes the software name/version and its authors. Some other info that may be
	/// returned includes the patch level and compile date of the (server), the copyright on the
	/// (server) software, and whatever miscellaneous information the (server) authors consider relevant.
	/// </summary>
	/// <format>INFO [(server)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_INFO"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFINFO"/><br/>
	/// </numerics>
	public sealed partial record INFO(string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to return documentation about the IRC server
	/// and the <see cref="IrcCommand"/>s it implements.
	/// </summary>
	/// <format>HELP [(subject)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_HELPNOTFOUND"/><br/>
	/// <see cref="IrcNumericReply.RPL_HELPSTART"/><br/>
	/// <see cref="IrcNumericReply.RPL_HELPTXT"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFHELP"/><br/>
	/// </numerics>
	public sealed partial record HELP(string? subject = null) : IrcCommand, IIrcCommand;
	#endregion

	#region Service Query and Commands
	/// <summary>
	/// This command is used to list services currently connected to
	/// the network and visible to the user issuing the command.
	/// </summary>
	/// <format>SERVLIST [(mask) [(type)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.RPL_SERVLIST"/><br/>
	/// <see cref="IrcNumericReply.RPL_SERVLISTEND"/><br/>
	/// </numerics>
	public sealed partial record SERVLIST(string? mask = null, string? type = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used similarly to <see cref="IrcCommand.PRIVMSG"/>. The only difference is that the
	/// recipient must be a service. This is the only way for a text message to be delivered to a service.
	/// </summary>
	/// <format>SQUERY (servicename) (text)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NORECIPIENT"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTEXTTOSEND"/><br/>
	/// <see cref="IrcNumericReply.ERR_CANNOTSENDTOCHAN"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOTOPLEVEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_WILDTOPLEVEL"/><br/>
	/// <see cref="IrcNumericReply.ERR_TOOMANYTARGETS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// </numerics>
	public sealed partial record SQUERY(string servicename, string text) : IrcCommand, IIrcCommand;
	#endregion

	#region User based queries
	/// <summary>
	/// This command is used to query a list of users who match the provided mask.
	/// If (ops) is specified, only operators are returned.
	/// </summary>
	/// <format>WHO [(mask) [o]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOREPLY"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFWHO"/><br/>
	/// </numerics>
	public sealed partial record WHO(string? mask = null, bool ops = false) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to query information about particular users. If (target) is
	/// specified, the request is forwarded to that server which will generate the reply.
	/// </summary>
	/// <format>WHOIS [(target)] (masks)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NONICKNAMEGIVEN"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISCERTFP"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISREGNICK"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISUSER"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISOPERATOR"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISIDLE"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISCHANNELS"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISSPECIAL"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISACCOUNT"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISACTUALLY"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISHOST"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISMODES"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISSECURE"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFWHOIS"/><br/>
	/// <see cref="IrcNumericReply.RPL_AWAY"/><br/>
	/// </numerics>
	public sealed partial record WHOIS(IEnumerable<string> masks, string? target = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command asks for information about a nickname which no longer exists. This may
	/// either be due to a nickname change or the user leaving IRC. The history is searched
	/// backward, returning the most recent entry first. If there are multiple entries,
	/// up to (count) replies will be returned (or all of them if (count) is not specified).
	/// </summary>
	/// <format>WHOWAS (nicknames) [(count) [(target)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NONICKNAMEGIVEN"/><br/>
	/// <see cref="IrcNumericReply.ERR_WASNOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOWASUSER"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISSERVER"/><br/>
	/// <see cref="IrcNumericReply.RPL_WHOISACTUALLY"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFWHOWAS"/><br/>
	/// </numerics>
	public sealed partial record WHOWAS(IEnumerable<string> nicknames, int? count = null, string? target = null) : IrcCommand, IIrcCommand;
	#endregion

	#region Miscellaneous messages
	/// <summary>
	/// This command is used to close the connection between a given client and the server they are connected
	/// to. This is a privileged command and is available only to IRC Operators. (nickname) represents the
	/// user to be "killed", and (comment) is shown to all users and to the user themselves upon being killed.
	/// </summary>
	/// <format>KILL (nickname) (comment)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_CANTKILLSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHNICK"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVS"/><br/>
	/// </numerics>
	public sealed partial record KILL(string nickname, string comment) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is sent by either clients or servers to check the other side of the connection is still
	/// connected and/or to check for connection latency, at the application layer. If (server2) is specified,
	/// it represents the target of the ping, and the message gets forwarded there.
	/// </summary>
	/// <format>PING (server1) [(server2)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOORIGIN"/><br/>
	/// </numerics>
	public sealed partial record PING(string server1, string? server2 = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used as a reply to <see cref="IrcCommand.PING"/> commands, by both clients and servers.
	/// </summary>
	/// <format>PONG (server) [(server2)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOORIGIN"/><br/>
	/// </numerics>
	public sealed partial record PONG(string server, string? server2 = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This message is sent from a server to a client to report a
	/// fatal error, before terminating the client's connection.
	/// </summary>
	/// <format>ERROR (message)</format>
	/// <numerics>None</numerics>
	public sealed partial record ERROR(string message) : IrcCommand, IIrcCommand;
	#endregion

	#region Optional features
	/// <summary>
	/// This command lets clients indicate that their user is away. If (comment) is specified,
	/// the user is set to be away. If comment is not specified, the user is no longer away.
	/// </summary>
	/// <format>AWAY [(comment)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.RPL_UNAWAY"/><br/>
	/// <see cref="IrcNumericReply.RPL_NOWAWAY"/><br/>
	/// </numerics>
	public sealed partial record AWAY(string? comment = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This is an administrative command which can be used by an operator
	/// to force the local server to re-read and process its configuration file.
	/// This may include other data, such as modules or TLS certificates.
	/// </summary>
	/// <format>REHASH</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// <see cref="IrcNumericReply.RPL_REHASHING"/><br/>
	/// </numerics>
	public sealed partial record REHASH : IrcCommand, IIrcCommand;

	/// <summary>
	/// This is an administrative command which can be used by an operator to shutdown the server.
	/// </summary>
	/// <format>DIE</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// </numerics>
	public sealed partial record DIE : IrcCommand, IIrcCommand;

	/// <summary>
	/// This is an administrative command which can be used by an operator to force the server to restart itself.
	/// </summary>
	/// <format>RESTART</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// </numerics>
	public sealed partial record RESTART : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command can be used to give users who are on a host running an IRC (server) a message asking them to join IRC.
	/// </summary>
	/// <format>SUMMON (user) [(server) [(channel)]]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_NORECIPIENT"/><br/>
	/// <see cref="IrcNumericReply.ERR_FILEERROR"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOLOGIN"/><br/>
	/// <see cref="IrcNumericReply.ERR_SUMMONDISABLED"/><br/>
	/// <see cref="IrcNumericReply.RPL_SUMMONING"/><br/>
	/// </numerics>
	public sealed partial record SUMMON(string user, string? server = null, string? channel = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command returns a list of users logged into the (server) in a format
	/// similar to the UNIX commands who(1), rusers(1) and finger(1).
	/// </summary>
	/// <format>USERS [(server)]</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NOSUCHSERVER"/><br/>
	/// <see cref="IrcNumericReply.ERR_FILEERROR"/><br/>
	/// <see cref="IrcNumericReply.ERR_USERSDISABLED"/><br/>
	/// <see cref="IrcNumericReply.RPL_NOUSERS"/><br/>
	/// <see cref="IrcNumericReply.RPL_USERSSTART"/><br/>
	/// <see cref="IrcNumericReply.RPL_USERS"/><br/>
	/// <see cref="IrcNumericReply.RPL_ENDOFUSERS"/><br/>
	/// </numerics>
	public sealed partial record USERS(string? server = null) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to send a message to all currently connected users who have set the 'w' user mode for themselves.
	/// </summary>
	/// <format>WALLOPS (text)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVILEGES"/><br/>
	/// <see cref="IrcNumericReply.ERR_NOPRIVS"/><br/>
	/// </numerics>
	public sealed partial record WALLOPS(string text) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command is used to return information about users with the given (nicknames).
	/// </summary>
	/// <format>USERHOST (nicknames)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.RPL_USERHOST"/><br/>
	/// </numerics>
	public sealed partial record USERHOST(IEnumerable<string> nicknames) : IrcCommand, IIrcCommand;

	/// <summary>
	/// This command was implemented to provide a quick and efficient means to
	/// get a response about whether a given (nickname) was currently on IRC.
	/// </summary>
	/// <format>ISON (nicknames)</format>
	/// <numerics>
	/// <see cref="IrcNumericReply.ERR_NEEDMOREPARAMS"/><br/>
	/// <see cref="IrcNumericReply.RPL_ISON"/><br/>
	/// </numerics>
	public sealed partial record ISON(IEnumerable<string> nicknames) : IrcCommand, IIrcCommand;
	#endregion

	/// <summary>
	/// This is not a normal command, but rather a reply sent be servers that contains a
	/// numeric and its associated arguments and should therefore not be sent by clients.
	/// </summary>
	public sealed partial record RESPONSE(IrcNumericReply numeric, IEnumerable<string> args) : IrcCommand, IIrcCommand;

	/// <summary>
	/// Represents a command that is not known to this library.
	/// </summary>
	public sealed partial record RAW(string name, IEnumerable<string> args) : IrcCommand, IIrcCommand;
}