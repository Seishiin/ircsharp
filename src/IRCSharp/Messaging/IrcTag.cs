using System;
using System.Text;

namespace IRCSharp.Messaging;

/// <summary>
/// Tags consist of <see cref="Key"/>s and optional <see cref="Value"/>s.
/// </summary>
public class IrcTag : IEquatable<IrcTag>
{
	/// <summary>
	/// The key of the <see cref="IrcTag"/>.
	/// </summary>
	public string Key { get; }

	/// <summary>
	/// The value of the <see cref="IrcTag"/>. May be empty.
	/// </summary>
	public string Value { get; init; }

	internal IrcTag(string key)
	{
		Key = key;
		Value = string.Empty;
	}

	internal IrcTag(string key, string escapedValue)
	{
		Key = key;
		Value = Unescape(escapedValue);
	}

	public bool Equals(IrcTag? other)
	{
		if (ReferenceEquals(null, other)) return false;
		if (ReferenceEquals(this, other)) return true;
		return Key == other.Key;
	}

	public override bool Equals(object? obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != typeof(IrcTag)) return false;
		return Equals((IrcTag)obj);
	}

	public override int GetHashCode() => Key.GetHashCode();
	public override string ToString() => string.IsNullOrEmpty(Value) ? Key : $"{Key}={Escape(Value)}";

	private static string Unescape(ReadOnlySpan<char> span)
	{
		var sb = new StringBuilder(span.Length);
		for (int i = 0; i < span.Length; i++)
		{
			if (span[i] == '\\')
			{
				if (++i >= span.Length) break;
				switch (span[i])
				{
					case ':':
						sb.Append(';');
						break;
					case 's':
						sb.Append(' ');
						break;
					case '\\':
						sb.Append('\\');
						break;
					case 'r':
						sb.Append('\r');
						break;
					case 'n':
						sb.Append('\n');
						break;
					default:
						sb.Append(span[i]);
						break;
				}
			}
			else
			{
				sb.Append(span[i]);
			}
		}

		return sb.ToString();
	}

	private static string Escape(ReadOnlySpan<char> span)
	{
		var sb = new StringBuilder(span.Length);
		foreach (char c in span)
		{
			switch (c)
			{
				case ';':
					sb.Append("\\:");
					break;
				case ' ':
					sb.Append("\\s");
					break;
				case '\\':
					sb.Append("\\\\");
					break;
				case '\r':
					sb.Append("\\r");
					break;
				case '\n':
					sb.Append("\\n");
					break;
				default:
					sb.Append(c);
					break;
			}
		}

		return sb.ToString();
	}
}