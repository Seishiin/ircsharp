﻿using IRCSharp.Modes;

namespace IRCSharp;

internal static class Utils
{
	public static bool IsChannel(string s) => s.StartsWith('#') || s.StartsWith('&') || s.StartsWith('+') || s.StartsWith('!');

	public static string Stringify(IrcModeSign sign) => sign switch { IrcModeSign.Plus => "+", IrcModeSign.Minus => "-", _ => string.Empty };
}