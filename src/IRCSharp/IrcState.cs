namespace IRCSharp;

internal sealed class IrcState
{
	private string nick;
	private string motd;

	private readonly object lockObject = new();
	private readonly string[] altNicks;
	private int altIndex;

	public IrcState(string nick, string[] altNicks)
	{
		this.nick = nick;
		this.altNicks = altNicks;
		motd = string.Empty;
	}

	public string GetNick()
	{
		lock (lockObject)
		{
			return nick;
		}
	}

	public void SetNick(string value)
	{
		lock (lockObject)
		{
			nick = value;
		}
	}

	public void UpdateNick()
	{
		lock (lockObject)
		{
			if (altNicks.Length > altIndex)
				nick = altNicks[altIndex++];
			else
				nick += "_";
		}
	}

	public string GetMotd()
	{
		lock (lockObject)
		{
			return motd;
		}
	}

	public void AppendMotdLine(string? line)
	{
		if (line == null) return;
		lock (lockObject)
		{
			if (motd != string.Empty)
				motd += '\n';

			motd += line;
		}
	}
}