using System;
using IRCSharp.Messaging;

namespace IRCSharp.EventArgs;

/// <summary>
/// Provides data for the <see cref="IrcClient.MessageReceived"/> event.
/// It consists of a <see cref="Message"/> and <see cref="ReceivedAt"/>.
/// </summary>
public sealed class MessageReceivedEventArgs : System.EventArgs
{
	/// <summary> <inheritdoc cref="IrcMessage"/> </summary>
	public IrcMessage Message { get; }

	/// <summary>
	/// Represents the time at which the <see cref="Message"/> was received.
	/// </summary>
	public DateTimeOffset ReceivedAt { get; }

	internal MessageReceivedEventArgs(IrcMessage msg)
	{
		Message = msg;
		ReceivedAt = DateTimeOffset.Now;
	}
}