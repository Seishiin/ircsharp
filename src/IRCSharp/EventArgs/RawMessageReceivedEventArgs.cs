using System;

namespace IRCSharp.EventArgs;

/// <summary>
/// Provides data for the <see cref="IrcClient.RawMessageReceived"/> event.
/// It consists of a <see cref="Message"/> and <see cref="ReceivedAt"/>.
/// </summary>
public sealed class RawMessageReceivedEventArgs : System.EventArgs
{
	/// <summary>
	/// Represents a raw message.
	/// </summary>
	public string Message { get; }

	/// <summary>
	/// Represents the time at which the <see cref="Message"/> was received.
	/// </summary>
	public DateTimeOffset ReceivedAt { get; }

	internal RawMessageReceivedEventArgs(string msg)
	{
		Message = msg;
		ReceivedAt = DateTimeOffset.Now;
	}
}