﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using IRCSharp.EventArgs;
using IRCSharp.Messaging;
using IRCSharp.Parsing;

namespace IRCSharp;

/// <summary>
/// Represents a client that communicates with a server using the IRC (Internet Relay Chat) protocol.
/// Has built-in support for <see cref="IrcNumericReply.RPL_ISUPPORT"/>, <see cref="IrcCommand.CTCP"/>,
/// <see cref="IrcCommand.PING"/> and nick collision handling. <see cref="IrcMessage"/>s can be sent
/// using <see cref="Send(IrcCommand)"/> or <see cref="Send(ISet{IrcTag}, IrcCommand)"/>.
/// </summary>
public sealed class IrcClient : IDisposable
{
	/// <summary>
	/// Occurs when a valid <see cref="IrcMessage"/> is received.
	/// Malformed messages are silently ignored.
	/// </summary>
	public event EventHandler<MessageReceivedEventArgs>? MessageReceived;

	/// <summary>
	/// Occurs when a raw message is received.
	/// </summary>
	public event EventHandler<RawMessageReceivedEventArgs>? RawMessageReceived;

	/// <summary>
	/// Represents the nickname of this client. This can be the original
	/// <see cref="IrcConfig.NickName"/> or one of the <see cref="IrcConfig.AltNickNames"/>.
	/// If the nickname is still taken, an underscore will be appended until it is valid.
	/// </summary>
	/// <remarks>
	/// A server can change the nickname at any time. Should this occur,
	/// then this value will be updated accordingly.
	/// </remarks>
	public string Nick => state.GetNick();

	/// <summary>
	/// Represents the server's "Message of the Day" or
	/// <see cref="string.Empty"/> if not provided.
	/// </summary>
	public string Motd => state.GetMotd();

	/// <summary> <inheritdoc cref="IrcServerOptions"/> </summary>
	public IrcServerOptions ServerOptions { get; }

	internal readonly IrcConfig Config;

	private Stream? stream;
	private StreamWriter? writer;
	private StreamReader? reader;
	private bool registered;

	private readonly MessageParser parser;
	private readonly CtcpHandler ctcpHandler;
	private readonly IrcState state;

	private readonly TcpClient tcpClient;
	private readonly CancellationTokenSource listenerTokenSource;

	internal IrcClient(IrcConfig config)
	{
		ServerOptions = new IrcServerOptions();
		Config = config;

		parser = new MessageParser(config.CmdMap);
		ctcpHandler = new CtcpHandler(this);
		state = new IrcState(config.NickName, config.AltNickNames);

		tcpClient = new TcpClient();
		listenerTokenSource = new CancellationTokenSource();
	}

	/// <summary>
	/// Connects to a server and registers using the provided <see cref="IrcConfig"/>.
	/// </summary>
	public async Task Connect()
	{
		await tcpClient.ConnectAsync(Config.ServerAddress, Config.Port).Ctx();

		if (Config.UseTls)
		{
			stream = new SslStream(tcpClient.GetStream());
			await ((SslStream)stream).AuthenticateAsClientAsync(Config.ServerAddress).Ctx();
		}
		else
		{
			stream = tcpClient.GetStream();
		}

		reader = new StreamReader(stream);
		writer = new StreamWriter(stream) { NewLine = "\r\n" };

		_ = ReceiveData(listenerTokenSource.Token);
		await Register().Ctx();
	}

	private async Task ReceiveData(CancellationToken token)
	{
		while (true)
		{
			try
			{
				string? line = await reader!.ReadLineAsync(token).Ctx();
				if (string.IsNullOrEmpty(line)) continue;

				RawMessageReceived?.Invoke(this, new RawMessageReceivedEventArgs(line));
				var msg = parser.Parse(line);
				await HandleMessage(msg).Ctx();
				MessageReceived?.Invoke(this, new MessageReceivedEventArgs(msg));
			}
			catch (OperationCanceledException) { }
			catch (FormatException) { }
		}
	}

	private async Task HandleMessage(IrcMessage msg)
	{
		switch (msg.Command)
		{
			case IrcCommand.PING cmd:
				await HandlePing(cmd).Ctx();
				break;
			case IrcCommand.NICK cmd:
				state.SetNick(cmd.nickname);
				break;
			case IrcCommand.RESPONSE { numeric: IrcNumericReply.RPL_MOTD } cmd:
				state.AppendMotdLine(cmd.args.LastOrDefault());
				break;
			case IrcCommand.RESPONSE { numeric: IrcNumericReply.RPL_ENDOFMOTD }:
			case IrcCommand.RESPONSE { numeric: IrcNumericReply.ERR_NOMOTD }:
				registered = true;
				break;
			case IrcCommand.RESPONSE { numeric: IrcNumericReply.ERR_NICKNAMEINUSE }:
			case IrcCommand.RESPONSE { numeric: IrcNumericReply.ERR_NICKCOLLISION }:
				await HandleNickCollision().Ctx();
				break;
			case IrcCommand.RESPONSE { numeric: IrcNumericReply.RPL_ISUPPORT } cmd:
				ServerOptions.ParseISupport(cmd);
				break;
			case IrcCommand.CTCP { type: CtcpType.Query } cmd:
				await ctcpHandler.HandleQuery(msg, cmd).Ctx();
				break;
		}
	}

	private async Task HandlePing(IrcCommand.PING ping)
	{
		await Send(new IrcCommand.PONG(ping.server1)).Ctx();
	}

	private async Task HandleNickCollision()
	{
		state.UpdateNick();
		await Send(new IrcCommand.NICK(Nick)).Ctx();
	}

	private async Task Register()
	{
		if (!string.IsNullOrEmpty(Config.Password))
			await Send(new IrcCommand.PASS(Config.Password)).Ctx();

		await Send(new IrcCommand.NICK(Nick)).Ctx();
		await Send(new IrcCommand.USER(Config.UserName, 0, Config.RealName)).Ctx();

		while (!registered)
			await Task.Delay(500).Ctx();
	}

	/// <summary>
	/// Sends a command.
	/// </summary>
	/// <param name="command">The <see cref="IrcCommand"/> to send.</param>
	public async Task Send(IrcCommand command)
	{
		if (writer == null) return;

		await writer.WriteLineAsync(command.ToString()).Ctx();
		await writer.FlushAsync().Ctx();
	}

	/// <summary>
	/// Sends tags and a command.
	/// </summary>
	/// <param name="tags">The <see cref="IrcTag"/>s to send.</param>
	/// <param name="command">The <see cref="IrcCommand"/> to send.</param>
	public async Task Send(ISet<IrcTag> tags, IrcCommand command)
	{
		if (writer == null) return;

		var msg = new IrcMessage(command) { Tags = tags };
		await writer.WriteLineAsync(msg.ToString()).Ctx();
		await writer.FlushAsync().Ctx();
	}

	/// <summary>
	/// Sends a <see cref="IrcCommand.QUIT"/> command and disposes this <see cref="IrcClient"/> instance.
	/// </summary>
	public async Task Close()
	{
		await Send(new IrcCommand.QUIT()).Ctx();
		Dispose();
	}

	/// <summary>
	/// Releases all resources used by the current instance of the <see cref="IrcClient"/> class.
	/// </summary>
	public void Dispose()
	{
		listenerTokenSource.Cancel();
		listenerTokenSource.Dispose();
		writer?.Close();
		reader?.Close();
		tcpClient.Close();
	}

	/// <summary>
	/// Creates a new <see cref="IrcConfig"/> using the provided nickname and server address.
	/// </summary>
	/// <param name="nick">The <see cref="IrcClient"/>'s nickname.</param>
	/// <param name="server">The server to connect to.</param>
	/// <returns>An <see cref="IrcConfig"/> used to create an <see cref="IrcClient"/>.</returns>
	public static IrcConfig New(string nick, string server) => new(nick, server);
}