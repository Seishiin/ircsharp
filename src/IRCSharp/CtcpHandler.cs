using System;
using System.Threading.Tasks;
using IRCSharp.Messaging;

namespace IRCSharp;

internal sealed class CtcpHandler
{
	private const string Finger = "FINGER";
	private const string Ping = "PING";
	private const string Time = "TIME";
	private const string Version = "VERSION";
	private const string Source = "SOURCE";
	private const string UserInfo = "USERINFO";

	private readonly IrcClient client;

	public CtcpHandler(IrcClient client)
	{
		this.client = client;
	}

	public async Task HandleQuery(IrcMessage msg, IrcCommand.CTCP ctcp)
	{
		string? target = msg.ResponseTarget();
		if (string.IsNullOrEmpty(target)) return;

		switch (ctcp.command)
		{
			case Finger:
				await HandleFinger(target).Ctx();
				break;
			case Ping:
				await HandlePing(target, ctcp).Ctx();
				break;
			case Time:
				await HandleTime(target).Ctx();
				break;
			case Version:
				await HandleVersion(target).Ctx();
				break;
			case Source:
				await HandleSource(target).Ctx();
				break;
			case UserInfo:
				await HandleUserInfo(target).Ctx();
				break;
		}
	}

	private async Task HandleFinger(string target)
	{
		string text = $"{client.Config.RealName} ({client.Config.UserName})";
		await client.Send(Reply(target, Finger, text)).Ctx();
	}

	private async Task HandlePing(string target, IrcCommand.CTCP ctcp)
	{
		if (ctcp.text == null) return;
		await client.Send(Reply(target, Ping, ctcp.text)).Ctx();
	}

	private async Task HandleTime(string target)
	{
		string text = DateTime.Now.ToString("O");
		await client.Send(Reply(target, Time, text)).Ctx();
	}

	private async Task HandleVersion(string target)
	{
		string text = client.Config.Version ?? "IRCSharp";
		await client.Send(Reply(target, Version, text)).Ctx();
	}

	private async Task HandleSource(string target)
	{
		string text = client.Config.Source ?? "Unspecified";
		await client.Send(Reply(target, Source, text)).Ctx();
	}

	private async Task HandleUserInfo(string target)
	{
		string text = client.Config.UserInfo ?? "Unspecified";
		await client.Send(Reply(target, UserInfo, text)).Ctx();
	}

	private static IrcCommand.CTCP Reply(string target, string command, string text)
		=> new(CtcpType.Reply, target, command, text);
}